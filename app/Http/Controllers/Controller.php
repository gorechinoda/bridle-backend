<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Bridle OpenApi Documentation",
     *      description="Bridle OpenApi description",
     *      @OA\Contact(
     *          email="gorechinoda@gmail.com"
     *      ),
     *      @OA\License(
     *          name="Apache",
     *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *      )
     * )
     *
     * @OA\Server(
     *      url="http://bridle/api",
     *      description="Bridle API Server"
     * )

     *
     * @OA\Tag(
     *     name="Bridle",
     *     description="API Endpoints of Bridle Backend"
     * )
     */
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
