<?php

namespace App\Http\Controllers;

use App\model\contact\TbladditionalContacts;
use App\model\contact\TblcontactType;
use Illuminate\Http\Request;

class TbladditionalContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return TblcontactType::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            "contact_type_id"=>["required"],
            "client_id"=>['required'],
        ]);
        return TbladditionalContacts::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\TbladditionalContacts  $tbladditionalContacts
     * @return \Illuminate\Http\Response
     */
    public function show(TbladditionalContacts $tbladditionalContacts)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\model\TbladditionalContacts  $tbladditionalContacts
     * @return \Illuminate\Http\Response
     */
    public function edit(TbladditionalContacts $tbladditionalContacts)
    {
        //
        return TblcontactType::all();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\TbladditionalContacts  $tbladditionalContacts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tbladditionalContacts)
    {
        //
        $tbladditionalContacts=TbladditionalContacts::find($tbladditionalContacts);
        if($tbladditionalContacts){
            return $tbladditionalContacts->update($request->all());
        }

        return ["error"=>true,"errorMessage"=>"Contacts not found!"];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\TbladditionalContacts  $tbladditionalContacts
     * @return \Illuminate\Http\Response
     */
    public function destroy( $tbladditionalContacts)
    {
        //
        $tbladditionalContacts=TbladditionalContacts::find($tbladditionalContacts);
        if($tbladditionalContacts){
            return $tbladditionalContacts->delete();
        }

        return ["error"=>true,"errorMessage"=>"Contacts not found!"];
    }
}
