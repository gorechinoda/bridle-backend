<?php

namespace App\Http\Controllers;

use App\model\consent\TblclientConsent;
use Illuminate\Http\Request;
use App\model\consent\TblconsentType;

class TblclientConsentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // return TblconsentType::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return TblconsentType::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            "consent_type_id"=>['required'],
            "client"=>['required'],
            "expiry_date"=>['required'],
        ]);

        TblclientConsent::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\TblclientConsent  $tblclientConsent
     * @return \Illuminate\Http\Response
     */
    public function show($tblclientConsent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\model\TblclientConsent  $tblclientConsent
     * @return \Illuminate\Http\Response
     */
    public function edit($tblclientConsent)
    {
        //
        return TblconsentType::all();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\TblclientConsent  $tblclientConsent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tblclientConsent)
    {
        //
        $tblclientConsent = TblclientConsent::find($tblclientConsent);
        if($tblclientConsent){
            $tblclientConsent->update($request->all());
            return $tblclientConsent;
        }

        return ["error"=>true,"errorMessage"=>"Client consent not found"];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\TblclientConsent  $tblclientConsent
     * @return \Illuminate\Http\Response
     */
    public function destroy($tblclientConsent)
    {
        //
        $tblclientConsent = TblclientConsent::find($tblclientConsent);
        if($tblclientConsent){
            return $tblclientConsent->delete();
        }

        return ["error"=>true,"errorMessage"=>"Client consent not found"];
    }
}
