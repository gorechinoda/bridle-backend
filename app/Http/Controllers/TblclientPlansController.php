<?php

namespace App\Http\Controllers;

use App\model\plan\TblclientPlans;
use Illuminate\Http\Request;
use App\model\plan\TblplanType;

class TblclientPlansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return TblplanType::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'plan_type_id'=>['required'],
            'ndis_number'=>['required'],
            'start_date'=>['required'],
            'end_date'=>['required'],
        ]);
        return TblclientPlans::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\TblclientPlans  $tblclientPlans
     * @return \Illuminate\Http\Response
     */
    public function show( $tblclientPlans)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\model\TblclientPlans  $tblclientPlans
     * @return \Illuminate\Http\Response
     */
    public function edit($tblclientPlans)
    {
        //
        return TblplanType::all();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\TblclientPlans  $tblclientPlans
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tblclientPlans)
    {
        //
        $tblclientPlans = TblclientPlans::find($tblclientPlans);
        if($tblclientPlans){
            $tblclientPlans->update($request->all());
            return $tblclientPlans;
        }
        return ['error'=>true,'errorMessage'=>"Client plan not found"];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\TblclientPlans  $tblclientPlans
     * @return \Illuminate\Http\Response
     */
    public function destroy(TblclientPlans $tblclientPlans)
    {
        //
        $tblclientPlans = TblclientPlans::find($tblclientPlans);
        if($tblclientPlans){
            return $tblclientPlans->delete();
        }
        return ['error'=>true,'errorMessage'=>"Client plan not found"];
    }
}
