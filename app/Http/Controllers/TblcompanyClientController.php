<?php

namespace App\Http\Controllers;

use App\model\TblcompanyClient;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class TblcompanyClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $clients = TblcompanyClient::all();
        foreach ($clients as $value) {
            # code...
            $value->user;
        }
        return $clients;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @OA\Post(
     *      path="/client",
     *      operationId="Create  client",
     *      tags={"Client"},
     *      summary="Create Client",
     *      description="Returns client data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/TblcompanyClient")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TblcompanyClient")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // return $request;
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'email'=>['required','unique:users'],
            'surname'=>'required',
            // 'password'=>'required',
            'date_of_birth'=>'required',
            'company_id'=>'required',
            'username'=>['required','unique:users']
        ]);
        if($validator->fails()){
            return ["error"=>true,"errors"=>$validator->errors()];
        }
        $request["password"] = Hash::make($request['username']);
        $user = User::create($request->all());

        $tblcompanyClient = TblcompanyClient::create(
            [
                'company_id'=>$request['company_id'],
                'users_id'=>$user->id,
            ]
        );

        $tblcompanyClient->company;
        $tblcompanyClient->user;
        return $tblcompanyClient;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\TblcompanyClient  $tblcompanyClient
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tblcompanyClient = TblcompanyClient::find($id);
        if(!$tblcompanyClient){
            return ["error"=>true,"errorMessage"=>"Client with id=$id does not exist"];
        }
        $tblcompanyClient->company;
        $tblcompanyClient->user;
        return $tblcompanyClient;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\model\TblcompanyClient  $tblcompanyClient
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tblcompanyClient = TblcompanyClient::find($id);
        if(!$tblcompanyClient){
            return ["error"=>true,"errorMessage"=>"Client with id=$id does not exist"];
        }
        $tblcompanyClient->company;
        $tblcompanyClient->user;
        return $tblcompanyClient;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\TblcompanyClient  $tblcompanyClient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $tblcompanyClient = TblcompanyClient::find($id);
        if(!$tblcompanyClient){
            return ["error"=>true,"errorMessage"=>"Client with id=$id does not exist"];
        }
        $tblcompanyClient->update($request->all());

        $tblcompanyClient->company;
        $tblcompanyClient->user;
        return $tblcompanyClient;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\TblcompanyClient  $tblcompanyClient
     * @return \Illuminate\Http\Response
     */

      /**
     * @OA\Delete(
     *      path="/client/clientId",
     *      operationId="Delete  client",
     *      tags={"Client"},
     *      summary="Delete Client",
     *      description="Removes client data",
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function destroy($id)
    {
        $tblcompanyClient = TblcompanyClient::find($id);
        if(!$tblcompanyClient){
            return ["error"=>true,"errorMessage"=>"Client with id=$id does not exist"];
        }
        return $this->delete($tblcompanyClient);
    }


     /**
     * @OA\Delete(
     *      path="/client/id/byUserId",
     *      operationId="Delete  client using userId",
     *      tags={"Client"},
     *      summary="Delete Client using user id",
     *      description="Removes client data",
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function destroyByUserId($id)
    {
        $tblcompanyClient = TblcompanyClient::where('users_id',$id)->get();
        if($tblcompanyClient->isEmpty()){
            return ["error"=>true,"errorMessage"=>"Client with id=$id does not exist"];
        }
        return $this->delete($tblcompanyClient->first());
    }


    private function delete($wow){

        $user = User::find($wow->users_id);
        $rezo = $wow->delete();
        if(!$user){
            $user->delete();
        }
        return ['deleted'=>$rezo];
    }
}
