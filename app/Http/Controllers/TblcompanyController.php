<?php

namespace App\Http\Controllers;

use App\model\TblemplyeeRange;
use App\model\Tblcompany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\model\TblcompanyStaff;

class TblcompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function index()
    {
        $companies = Tblcompany::all();
        foreach ($companies as $value) {
            $value->employee_range;
        }
        return $companies;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return TblemplyeeRange::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     /**
     * @OA\Post(
     *      path="/company",
     *      operationId="storeCompany",
     *      tags={"Company"},
     *      summary="Store new Company",
     *      description="Returns Company data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Tblcompany")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Tblcompany")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'company_name' => 'required',
            'company_email'=>['required','unique:tblcompanies'],
            'company_mobile'=>'required',
            'employee_range_id'=>'required',
        ]);
        if($validator->fails()){
            return ['error'=>true,"errorMessage"=>"Provided fields are not meeting the required format",
                    "fields"=>$validator->errors()];
        }
        $companyInfor = Tblcompany::create($request->all());
        $companyInfor->save();
        return $companyInfor;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\Tblcompany  $tblcompany
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Get(
     *      path="/company/{id}",
     *      operationId="getCompany",
     *      tags={"Company"},
     *      summary="Get company information",
     *      description="Returns company data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Company ID",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Tblcompany")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function show($id)
    {
        //
        $tblcompany= Tblcompany::find($id);
        if(!$tblcompany){
            return ['error'=>true,"errorMessage"=>"Company with id = $id does not exist"];
        }
        $tblcompany->employee_range;
        foreach($tblcompany->staff as $staff){
            $staff->user;
        }
        foreach($tblcompany->clients as $client){
            $staff->user;
        }
        $tblcompany->clients;
        $tblcompany->licenses;
        $tblcompany->timesheets;
        return $tblcompany;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\model\Tblcompany  $tblcompany
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->show($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\Tblcompany  $tblcompany
     * @return \Illuminate\Http\Response
     */

     /**
     * @OA\Put(
     *      path="/company/{id}",
     *      operationId="updateCompany",
     *      tags={"Company"},
     *      summary="Update existing company",
     *      description="Returns updated company data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Company id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Tblcompany")
     *      ),
     *      @OA\Response(
     *          response=202,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Tblcompany")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function update(Request $request, $tblcompany)
    {
        //
        $validatedData = $request->validate([
            'company_name' => 'required',
            'company_email'=>['required'],
            'company_mobile'=>'required',
            'employee_range_id'=>'required',
            'id'=>['required'],
        ]);
        // $request->status=0;
        $companyInfor = Tblcompany::update($request->all());
        // $companyInfor->save();
        return $companyInfor;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\Tblcompany  $tblcompany
     * @return \Illuminate\Http\Response
     */

     /**
     * @OA\Delete(
     *      path="/company/{id}",
     *      operationId="deleteCompany",
     *      tags={"Company"},
     *      summary="Delete existing company",
     *      description="Deletes a record and returns no content",
     *      @OA\Parameter(
     *          name="id",
     *          description="Company id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function destroy($id)
    {
        //
        $tblcompany= $this->show($id);
        return ['deleted'=>$tblcompany->delete()];
    }


    /**
     * @OA\Post(
     *      path="/check",
     *      operationId="checkCompany",
     *      tags={"Company"},
     *      summary="Create Roster",
     *      description="Returns Roster data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="company_name",
     *                  type="string"
     *              ),
     *               @OA\Property(
     *                  property="company_email",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Tblcompany")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function checkCompany(Request $request){
        // return "Hey";
        $validatedData = $request->validate([
            'company_name' => 'required',
            'company_email'=>['required'],
        ]);
        $tblcompany = Tblcompany::where("company_email",$request->company_email)->first();
        if($tblcompany && $tblcompany->id){
            return ["exist"=>true];
        }else{
            return ["exist"=>false];
        }
    }

    /**
     * @OA\Get(
     *      path="/company/{id}/staff",
     *      operationId="getCompanyStaff",
     *      tags={"Company"},
     *      summary="Get company information Staff",
     *      description="Returns company data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Company ID",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="companyStaff",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/TblcompanyStaff")
     *              ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function getCompanyStaff($id){
        $company = $this->show($id);
        if($company["error"]){
            return $company;
        }
        foreach ($company->staff as $staff) {
            $staff->user;
        }
        return ["companyStaff"=>$company->staff];
    }

    /**
     * @OA\Get(
     *      path="/company/{id}/activeStaff",
     *      operationId="getActiveCompanyStaff",
     *      tags={"Company"},
     *      summary="Get active company Staff",
     *      description="Returns company staff that are active",
     *      @OA\Parameter(
     *          name="id",
     *          description="Company ID",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="activeCompanyStaff",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/TblcompanyStaff")
     *              ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function getActiveCompanyStaff($id){
        return ["activeCompanyStaff"=>$this->getCompanyStaffByStatus($id,1)];
    }

     /**
     * @OA\Get(
     *      path="/company/{id}/inActiveStaff",
     *      operationId="getInActiveCompanyStaff",
     *      tags={"Company"},
     *      summary="Get in active company Staff",
     *      description="Returns company staff that are in active",
     *      @OA\Parameter(
     *          name="id",
     *          description="Company ID",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="inActiveCompanyStaff",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/TblcompanyStaff")
     *              ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function getInActiveCompanyStaff($id){
        return ["inActiveCompanyStaff"=>$this->getCompanyStaffByStatus($id,0)];
    }

    private function getCompanyStaffByStatus($companyId,$status){
        $company = $this->show($companyId);
        if($company["error"]){
            return $company;
        }
        $data = array();
        foreach ($company->staff as $staff) {
            if($staff->user->status==$status){
                array_push($data,$staff);
            }
        }

        return $data;
    }


    /**
     * @OA\Get(
     *      path="/company/{id}/clients",
     *      operationId="getCompanyClient",
     *      tags={"Company"},
     *      summary="Get company information Client",
     *      description="Returns company data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Company ID",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="companyClients",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/TblcompanyClient")
     *              ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function getCompanyClients($id){
        $company = $this->show($id);
        if($company["error"]){
            return $company;
        }
        foreach ($company->clients as $client) {
            $client->user;
        }
        return ["companyClients"=>$company->clients];
    }

    /**
     * @OA\Get(
     *      path="/company/{id}/licences",
     *      operationId="getCompanyLicense",
     *      tags={"Company"},
     *      summary="Get company licences",
     *      description="Returns company licences data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Company ID",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="companyLicences",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/TblcompanyLicense")
     *              ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function getCompanyLicense($id){
        $company = $this->show($id);
        if($company["error"]){
            return $company;
        }
        return ["companyLicences"=>$company->licenses];
    }


    /**
     * @OA\Get(
     *      path="/company/{id}/timesheets",
     *      operationId="getCompanyTimesheets",
     *      tags={"Company"},
     *      summary="Get company timesheets",
     *      description="Returns company timesheets",
     *      @OA\Parameter(
     *          name="id",
     *          description="Company ID",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="companyTimesheets",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Tbltimesheet")
     *              ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function getCompanyTimesheets($id){
        $company = $this->show($id);
        if($company["error"]){
            return $company;
        }

        return $company->timesheets;
    }


    /**
     * @OA\Get(
     *      path="/company/{id}/events",
     *      operationId="getCompanyEvents",
     *      tags={"Company"},
     *      summary="Get company events",
     *      description="Returns company events",
     *      @OA\Parameter(
     *          name="id",
     *          description="Company ID",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="events",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Tblevent")
     *              ),
     *          )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function getCompanyEvents($id){
        $company = $this->show($id);
        if(!$company["error"]){
            return ["events"=>$company->events];
        }

        return $company;
    }
}
