<?php

namespace App\Http\Controllers;

use App\model\TblcompanyLicense;
use Illuminate\Http\Request;

class TblcompanyLicenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\TblcompanyLicense  $tblcompanyLicense
     * @return \Illuminate\Http\Response
     */
    public function show(TblcompanyLicense $tblcompanyLicense)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\model\TblcompanyLicense  $tblcompanyLicense
     * @return \Illuminate\Http\Response
     */
    public function edit(TblcompanyLicense $tblcompanyLicense)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\TblcompanyLicense  $tblcompanyLicense
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TblcompanyLicense $tblcompanyLicense)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\TblcompanyLicense  $tblcompanyLicense
     * @return \Illuminate\Http\Response
     */
    public function destroy(TblcompanyLicense $tblcompanyLicense)
    {
        //
    }
}
