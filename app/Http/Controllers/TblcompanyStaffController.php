<?php

namespace App\Http\Controllers;

use App\model\TblcompanyStaff;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\model\Tblrole;
use Illuminate\Support\Facades\Validator;
use App\model\staff\TblstaffContact;
use App\model\staff\TblstaffProfessional;
use App\model\staff\TblstaffEducation;
use App\model\staff\TblstaffExpirience;
use App\model\staff\TblstaffDocument;
use App\model\staff\TblstaffEmploymentInformation;
use App\model\staff\TblstaffBankingInformation;
use App\model\staff\TblstaffReference;
use App\model\staff\TblstaffEmegencyContact;

use App\model\Tblcompany;

//wow
use App\model\staff\TblstaffDocumentType;
use App\model\staff\TblstaffEducationLevel;

class TblcompanyStaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $staffMembers = TblcompanyStaff::all();
        foreach ($staffMembers as $value) {
            $value->user;
            $value->company;
            $value->role;
        }

        return $staffMembers;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return Tblrole::all();
    }



    /**
     * @OA\Post(
     *      path="/companyStaff",
     *      operationId="create staff",
     *      tags={"Company Staff"},
     *      summary="Create staff for Staff",
     *      description="Returns User data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/TblcompanyStaff")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TblcompanyStaff")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'email'=>['required','unique:users'],
            'surname'=>'required',
            // 'password'=>'required',
            'date_of_birth'=>'required',
            'role_id'=>'required',
            'company_id'=>'required',
            'username'=>['required','unique:users']
        ]);
        if($validator->fails()){
            return ['errror'=>true,'errorMessage'=>'Fields not meeting the rqguired format','fields'=>$validator->errors()];
        }
        $request["password"] = $request['password'] ?? 'bridle@2020';

        $request["password"] = Hash::make($request['password']);

        $user = User::create($request->all());
        $request['users_id']= $user->id;
        $staff = TblcompanyStaff::create(
            $request->all()
        );

        $staff->company;
        $staff->role;
        $staff->user;
        return $staff;

    }

    /**
     * @OA\Put(
     *      path="/companyStaff/userId",
     *      operationId="update staff",
     *      tags={"Company Staff"},
     *      summary="Update staff using user id",
     *      description="please make use of User id to update and don't pass password please.",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/TblcompanyStaff")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TblcompanyStaff")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateStaff(Request $request,$id)
    {
        // return $request;
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'email'=>['required','unique:users'],
            'surname'=>'required',
            // 'password'=>'required',
            'date_of_birth'=>'required',
            'role_id'=>'required',
            'company_id'=>'required',
            'username'=>['required','unique:users']
        ]);
        if($validator->fails()){
            return ['errror'=>true,'errorMessage'=>'Fields not meeting the rqguired format','fields'=>$validator->errors()];
        }

        $user = User::find($id);
        $user = $user->update($request->all());
        $staff = TblcompanyStaff::where('users_id',$id)->get();

        if($staff->isEmpty()){
            $request['users_id'] = $user->id;
            $staff = TblcompanyStaff::create($request->all());
        }else{
            $staff = $staff->first()->update($request->all());
        }

        $staff->company;
        $staff->role;
        $staff->user;
        return $staff;

    }

    /**
     * @OA\Put(
     *      path="/companyStaff/changePassword",
     *      operationId="update staff password",
     *      tags={"Company Staff"},
     *      summary="Update staff password",
     *      description="this is for changing password",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="userId",
     *                  type="string",
     *              ),
     *              @OA\Property(
     *                  property="password",
     *                  type="string",
     *              ),
     *              @OA\Property(
     *                  property="oldPassword",
     *                  type="string",
     *              ),
     *          )
     *
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/User")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {
        // return $request;
        $validator = Validator::make($request->all(),[
            'userId' => 'required',
            'oldPassword'=>['required'],
            'password'=>'required'
        ]);
        if($validator->fails()){
            return ['errror'=>true,'errorMessage'=>'Fields not meeting the rqguired format','fields'=>$validator->errors()];
        }

        $user = User::find($request['userId']);
        if($user==null){
            return ['user not found'];
        }
        if(Hash::check($request['oldPassword'], $user->password)){
            $user->password =  Hash::make($request['password']);
            $user->save();
            return $user;
        }else{
            return ['password mismatch'];
        }

    }

/**
     * @OA\Post(
     *      path="/companyStaff/selfRegister/companyEmail",
     *      operationId="self register staff",
     *      tags={"Company Staff"},
     *      summary="Create staff for Company using email, ignore company_id",
     *      description="Returns User data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/TblcompanyStaff")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TblcompanyStaff")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function selfRegister($companyEmail,Request $request)
    {
        $company = Tblcompany::where("company_email",$companyEmail)->get();
        if ($company->isEmpty()){
            return ['error'=>"company with that email does not exist"];
        }
        $company = $company->first();
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'email'=>['required','unique:users'],
            'surname'=>'required',
            'password'=>'required',
            'date_of_birth'=>'required',
            'role_id'=>'required',
            'username'=>['required','unique:users'],
        ]);
        if($validator->fails()){
            return ['errror'=>true,'errorMessage'=>'Fields not meeting the rqguired format','fields'=>$validator->errors()];
        }

        $request["password"] = Hash::make($request['password']);
        $user = User::create($request->all());
        $staff = TblcompanyStaff::create(
            [
                'role_id'=>$request['role_id'],
                'company_id'=>$company->id,
                'users_id'=>$user->id,
            ]
        );

        $staff->company;
        $staff->role;
        $staff->user;
        return $staff;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\TblcompanyStaff  $tblcompanyStaff
     * @return \Illuminate\Http\Response
     */

    public function show($tblcompanyStaff)
    {
        $staff = TblcompanyStaff::find($tblcompanyStaff);
        if(!$staff){
            return ['error'=>true,'errorMessage'=>"Staff with id = $tblcompanyStaff not found"];
        }

        $staff->company;
        $staff->role;
        $staff->user;
        $staff->contacts;
        $staff->professionals;
        $staff->education;
        foreach ($staff->education as $value) {
            $value->educationLevel;
        }
        $staff->expirience;
        foreach ($staff->documents as $value) {
            $value->documentType;
        }
        $staff->employmentInformation;
        $staff->bankingInformation;
        $staff->references;
        $staff->emegencyContacts;

        return $staff;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\model\TblcompanyStaff  $tblcompanyStaff
     * @return \Illuminate\Http\Response
     */
    public function edit(TblcompanyStaff $tblcompanyStaff)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\TblcompanyStaff  $tblcompanyStaff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TblcompanyStaff $tblcompanyStaff)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\TblcompanyStaff  $tblcompanyStaff
     * @return \Illuminate\Http\Response
     */
    public function destroy(TblcompanyStaff $tblcompanyStaff)
    {
        //
    }

      /**
     * @OA\Delete(
     *      path="/companyStaff/staffId/remove",
     *      operationId="Delete  staff by staffId",
     *      tags={"Company Staff"},
     *      summary="Delete staff using staff id",
     *      description="Removes staff data",
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function delete($id){
        $wow = TblcompanyStaff::find($id);
        if(!$wow){
            return ["error"=>true,"errorMessage"=>"Staff with id=$id does not exist"];
        }
        return $this->deleteIt($wow);
    }

     /**
     * @OA\Delete(
     *      path="/companyStaff/byUserId/id",
     *      operationId="Delete  Staff using user id",
     *      tags={"Company Staff"},
     *      summary="Delete staff",
     *      description="Removes staff data",
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function deleteByUserId($id){
        $wow = TblcompanyStaff::where('users_id',$id)->get();
        if($wow->isEmpty()){
            return ["error"=>true,"errorMessage"=>"Staff with user id=$id does not exist"];
        }
        return $this->deleteIt($wow->first());
    }

    private function deleteIt($wow){

        $user = User::find($wow->users_id);
        $rezo = $wow->delete();
        if(!$user){
            $user->delete();
        }
        return ['deleted'=>$rezo];
    }

    /**
     * @OA\Post(
     *      path="/login",
     *      operationId="Login",
     *      tags={"Staff Login"},
     *      summary="Login to the system",
     *      description="Returns User data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="email",
     *                  type="string",
     *              ),
     *              @OA\Property(
     *                  property="password",
     *                  type="string",
     *              ),
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TblcompanyStaff")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function login(Request $request){
        $validator =Validator::make($request->all(),[
            'email'=>['required'],
            'password'=>'required',
        ]);

        if($validator->fails()){
            return ["error"=>true,"errors"=>$validator->errors()];
        }

        $user = User::where(['email'=>$request['email']])->first();
        // return $user;
        if(!$user){
            return ['error'=>true,'errorMessage'=>"user not found"];
        }

        if(Hash::check($request['password'], $user->password)){
            $staff = TblcompanyStaff::where('users_id',$user->id)->first();
            if($staff && $staff->id){
                $staff->company;
                $staff->role;
                $staff->user;
                return $staff;
            }
        }

        return ['error'=>true,'errorMessage'=>"user not found"];
    }

    ////////////////////Additional Staff information down here//////////////

    //TblstaffContact
    public function createStaffContact(){

    }



    /**
     * @OA\Post(
     *      path="/companyStaff/contact",
     *      operationId="TblstaffContactCreation",
     *      tags={"Company Staff"},
     *      summary="Create staff Contact",
     *      description="Returns created staff Contact data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/TblstaffContact"),
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TblstaffContact")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function saveStaffContact(Request $request){
        $validator =Validator::make($request->all(),[
            'staff_id'=>['required'],
        ]);

        if($validator->fails()){
            return ["error"=>true,"errors"=>$validator->errors()];
        }
        $request['id']=null;
        return TblstaffContact::create($request->all());

    }

    public function updateStaffContact(Request $request){
        $validator =Validator::make($request->all(),[
            'staff_id'=>['required'],
            'id'=>['required'],
        ]);

        if($validator->fails()){
            return ["error"=>true,"errors"=>$validator->errors()];
        }

        return TblstaffContact::update($request->all());
    }

    public function getStaffContactsFor($staffId){
        $staff = TblcompanyStaff::find($staffId);
        if($staff){
            $staff->contacts;
            return $staff;
        }
        return ['error'=>true,'errorMessage'=>"Could not find the staff with $staffId"];
    }

    public function destroyStaffContact($id){
        $data = TblstaffContact::find($id);
        if($data){
            return ['deleted'=>$data->delete()];
        }

        return ['error'=>true,'errorMessage'=>'Ccontact does not exist'];
    }

    // TblstaffProfessional
    public function createStaffProfessional(){

    }


    /**
     * @OA\Post(
     *      path="/companyStaff/professional",
     *      operationId="TblstaffProfessionalCreation",
     *      tags={"Company Staff"},
     *      summary="Create staff Professional",
     *      description="Returns created staff Professional data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/TblstaffProfessional"),
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TblstaffProfessional")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function saveStaffProfessional(Request $request){
        $validator =Validator::make($request->all(),[
            'staff_id'=>['required'],
        ]);

        if($validator->fails()){
            return ["error"=>true,"errors"=>$validator->errors()];
        }
        $request['id']=null;
        return TblstaffProfessional::create($request->all());

    }

    public function updateStaffProfessional(Request $request){
        $validator =Validator::make($request->all(),[
            'staff_id'=>['required'],
            'id'=>['required'],
        ]);

        if($validator->fails()){
            return ["error"=>true,"errors"=>$validator->errors()];
        }

        return TblstaffProfessional::update($request->all());
    }

    public function getStaffProfessionalsFor($staffId){
        $staff = TblcompanyStaff::find($staffId);
        if($staff){
            $staff->professionals;
            return $staff;
        }
        return ['error'=>true,'errorMessage'=>"Could not find the staff with $staffId"];
    }

    public function destroyStaffProfession($id){
        $data = TblstaffProfessional::find($id);
        if($data){
            return ['deleted'=>$data->delete()];
        }

        return ['error'=>true,'errorMessage'=>'Profession does not exist'];
    }


    // TblstaffEducation;
    public function createStaffStaffEducation(){
        return TblstaffEducationLevel::all();
    }


    /**
     * @OA\Post(
     *      path="/companyStaff/education",
     *      operationId="TblstaffEducationCreation",
     *      tags={"Company Staff"},
     *      summary="Create staff Education",
     *      description="Returns created staff Education data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/TblstaffEducation"),
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TblstaffEducation")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function saveStaffEducation(Request $request){
        $validator =Validator::make($request->all(),[
            'staff_id'=>['required'],
            'education_level_id'=>['required'],
        ]);

        if($validator->fails()){
            return ["error"=>true,"errors"=>$validator->errors()];
        }
        $request['id']=null;
        return TblstaffEducation::create($request->all());

    }

    public function updateStaffEducation(Request $request){
        $validator =Validator::make($request->all(),[
            'staff_id'=>['required'],
            'education_level_id'=>['required'],
            'id'=>['required'],
        ]);

        if($validator->fails()){
            return ["error"=>true,"errors"=>$validator->errors()];
        }

        return TblstaffEducation::update($request->all());
    }

    public function getStaffEducationFor($staffId){
        $staff = TblcompanyStaff::find($staffId);
        if($staff){
            $staff->education;
            foreach ($staff->education as $value) {
                # code...
                $value->educationLevel;
            }
            return $staff;
        }
        return ['error'=>true,'errorMessage'=>"Could not find the staff with $staffId"];
    }

    public function destroyStaffEducation($id){
        $data = TblstaffEducation::find($id);
        if($data){
            return ['deleted'=>$data->delete()];
        }

        return ['error'=>true,'errorMessage'=>'Education does not exist'];
    }


    // TblstaffExpirience;
    public function createStaffExpirience(){

    }


    /**
     * @OA\Post(
     *      path="/companyStaff/expirience",
     *      operationId="staffExpirienceCreation",
     *      tags={"Company Staff"},
     *      summary="Create staff Expirience",
     *      description="Returns created staff Expirience data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/TblstaffExpirience"),
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TblstaffExpirience")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function saveStaffExpirience(Request $request){
        $validator =Validator::make($request->all(),[
            'staff_id'=>['required'],
        ]);

        if($validator->fails()){
            return ["error"=>true,"errors"=>$validator->errors()];
        }
        $request['id']=null;
        return TblstaffExpirience::create($request->all());

    }

    public function updateStaffExpirience(Request $request){
        $validator =Validator::make($request->all(),[
            'staff_id'=>['required'],
            'id'=>['required'],
        ]);

        if($validator->fails()){
            return ["error"=>true,"errors"=>$validator->errors()];
        }

        return TblstaffExpirience::update($request->all());
    }

    public function getStaffExpirienceFor($staffId){
        $staff = TblcompanyStaff::find($staffId);
        if($staff){
            $staff->expirience;
            return $staff;
        }
        return ['error'=>true,'errorMessage'=>"Could not find the staff with $staffId"];
    }

    public function destroyStaffExpirience($id){
        $data = TblstaffExpirience::find($id);
        if($data){
            return ['deleted'=>$data->delete()];
        }

        return ['error'=>true,'errorMessage'=>'Expirience does not exist'];
    }



    // TblstaffDocument;
    public function createStaffDocument(){
        return TblstaffDocumentType::all();
    }


     /**
     * @OA\Post(
     *      path="/companyStaff/document",
     *      operationId="staffDocumentCreation",
     *      tags={"Company Staff"},
     *      summary="Create Document for staff member",
     *      description="Returns staff Document data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/TblstaffDocument"),
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TblstaffDocument")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function saveStaffDocument(Request $request){
        $validator =Validator::make($request->all(),[
            'staff_id'=>['required'],
            'document_type_id'=>['required'],
        ]);

        if($validator->fails()){
            return ["error"=>true,"errors"=>$validator->errors()];
        }
        $request['id']=null;
        return TblstaffDocument::create($request->all());

    }

    public function updateStaffDocument(Request $request){
        $validator =Validator::make($request->all(),[
            'staff_id'=>['required'],
            'document_type_id'=>['required'],
            'id'=>['required'],
        ]);

        if($validator->fails()){
            return ["error"=>true,"errors"=>$validator->errors()];
        }

        return TblstaffDocument::update($request->all());
    }

    public function getStaffDocumentsFor($staffId){
        $staff = TblcompanyStaff::find($staffId);
        if($staff){
            $staff->emegencyContacts;
            foreach ($staff->documents as $value) {
                # code...
                $value->documentType;
            }
            return $staff;
        }
         return ['error'=>true,'errorMessage'=>"Could not find the staff with $staffId"];
    }

    public function destroyStaffDocument($id){
        $data = TblstaffDocument::find($id);
        if($data){
            return ['deleted'=>$data->delete()];
        }

        return ['error'=>true,'errorMessage'=>'Document does not exist'];
    }


    // TblstaffEmploymentInformation;
    public function createStaffEmploymentInformation(){

    }


     /**
     * @OA\Post(
     *      path="/companyStaff/employmentInformation",
     *      operationId="staffEmploymentInformationCreation",
     *      tags={"Company Staff"},
     *      summary="Create Employment Information for staff member",
     *      description="Returns staff Employment Information data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/TblstaffEmploymentInformation"),
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TblstaffEmploymentInformation")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function saveStaffEmploymentInformation(Request $request){
        $validator =Validator::make($request->all(),[
            'staff_id'=>['required'],
        ]);

        if($validator->fails()){
            return ["error"=>true,"errors"=>$validator->errors()];
        }
        $request['id']=null;
        return TblstaffEmploymentInformation::create($request->all());

    }

    public function updateStaffEmploymentInformation(Request $request){
        $validator =Validator::make($request->all(),[
            'staff_id'=>['required'],
            'id'=>['required'],
        ]);

        if($validator->fails()){
            return ["error"=>true,"errors"=>$validator->errors()];
        }

        return TblstaffEmploymentInformation::update($request->all());
    }

    public function getStaffEmploymentInformationFor($staffId){
        $staff = TblcompanyStaff::find($staffId);
        if($staff){
            $staff->employmentInformation;
            return $staff;
        }
         return ['error'=>true,'errorMessage'=>"Could not find the staff with $staffId"];
    }

    public function destroyStaffEmploymentInformation($id){
        $data = TblstaffEmploymentInformation::find($id);
        if($data){
            return ['deleted'=>$data->delete()];
        }

        return ['error'=>true,'errorMessage'=>'Education does not exist'];
    }

    // TblstaffBankingInformation;
    public function createStaffBankingInformation(){

    }


    /**
     * @OA\Post(
     *      path="/companyStaff/bankingInformation",
     *      operationId="staffBankingInformationCreation",
     *      tags={"Company Staff"},
     *      summary="Create Banking Information for staff member",
     *      description="Returns staff Banking Information data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/TblstaffBankingInformation"),
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TblstaffBankingInformation")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function saveStaffBankingInformation(Request $request){
        $validator =Validator::make($request->all(),[
            'staff_id'=>['required'],
        ]);

        if($validator->fails()){
            return ["error"=>true,"errors"=>$validator->errors()];
        }
        $request['id']=null;
        return TblstaffBankingInformation::create($request->all());

    }

    public function updateStaffBankingInformation(Request $request){
        $validator =Validator::make($request->all(),[
            'staff_id'=>['required'],
            'id'=>['required'],
        ]);

        if($validator->fails()){
            return ["error"=>true,"errors"=>$validator->errors()];
        }

        return TblstaffBankingInformation::update($request->all());
    }

    public function getStaffBankingInformationFor($staffId){
        $staff = TblcompanyStaff::find($staffId);
        if($staff){
            $staff->bankingInformation;
            return $staff;
        }
         return ['error'=>true,'errorMessage'=>"Could not find the staff with $staffId"];
    }

    public function destroyStaffBankingInformation($id){
        $data = TblstaffBankingInformation::find($id);
        if($data){
            return ['deleted'=>$data->delete()];
        }

        return ['error'=>true,'errorMessage'=>'Banking information does not exist'];
    }


    // TblstaffReference;
    public function createStaffReference(){

    }



    /**
     * @OA\Post(
     *      path="/companyStaff/reference",
     *      operationId="referenceCreation",
     *      tags={"Company Staff"},
     *      summary="Create reference for staff member",
     *      description="Returns reference data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/TblstaffReference"),
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TblstaffReference")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function saveStaffReference(Request $request){
        $validator =Validator::make($request->all(),[
            'staff_id'=>['required'],
        ]);

        if($validator->fails()){
            return ["error"=>true,"errors"=>$validator->errors()];
        }
        $request['id']=null;
        // return $request;
        return TblstaffReference::create($request->all());

    }

    public function updateStaffReference(Request $request){
        $validator =Validator::make($request->all(),[
            'staff_id'=>['required'],
            'id'=>['required'],
        ]);

        if($validator->fails()){
            return ["error"=>true,"errors"=>$validator->errors()];
        }

        return TblstaffReference::update($request->all());
    }

    public function getStaffReferenceFor($staffId){
        $staff = TblcompanyStaff::find($staffId);
        if($staff){
            $staff->references;
            return $staff;
        }
         return ['error'=>true,'errorMessage'=>"Could not find the staff with $staffId"];
    }

    public function destroyStaffReference($id){
        $data = TblstaffReference::find($id);
        if($data){
            return ['deleted'=>$data->delete()];
        }

        return ['error'=>true,'errorMessage'=>'Reference does not exist'];
    }

    // TblstaffEmegencyContact;
    public function createStaffEmegencyContact(){

    }

    /**
     * @OA\Post(
     *      path="/companyStaff/emegencyContact",
     *      operationId="emegencyContactCreation",
     *      tags={"Company Staff"},
     *      summary="Create emegency contact for staff member",
     *      description="Returns emegency contact data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/TblstaffEmegencyContact"),
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TblstaffEmegencyContact")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function saveStaffEmegencyContact(Request $request){
        $validator =Validator::make($request->all(),[
            'staff_id'=>['required'],
        ]);

        if($validator->fails()){
            return ["error"=>true,"errors"=>$validator->errors()];
        }
        $request['id']=null;
        return TblstaffEmegencyContact::create($request->all());

    }

    public function updateStaffEmegencyContact(Request $request){
        $validator =Validator::make($request->all(),[
            'staff_id'=>['required'],
            'id'=>['required'],
        ]);

        if($validator->fails()){
            return ["error"=>true,"errors"=>$validator->errors()];
        }

        return TblstaffEmegencyContact::update($request->all());
    }

    public function getStaffEmegencyContactsFor($staffId){
        $staff = TblcompanyStaff::find($staffId);
        if($staff){
            $staff->emegencyContacts;
            return $staff;
        }

         return ['error'=>true,'errorMessage'=>"Could not find the staff with $staffId"];
    }

    public function destroyStaffEmegencyContact($id){
        $data = TblstaffEmegencyContact::find($id);
        if($data){
            return ['deleted'=>$data->delete()];
        }

        return ['error'=>true,'errorMessage'=>'Emegency contact does not exist'];
    }

}
