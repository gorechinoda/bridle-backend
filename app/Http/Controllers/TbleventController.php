<?php

namespace App\Http\Controllers;

use App\model\Tblevent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TbleventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Tblevent::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @OA\Post(
     *      path="/events",
     *      operationId="createEvent",
     *      tags={"Events"},
     *      summary="Create event",
     *      description="Returns event data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Tblevent")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Tblevent")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(),[
            'title'=>['required'],
            'description'=>['required'],
            'start_date'=>['required'],
            'end_date'=>['required'],
            "company_id"=>["required"],
        ]);

        if($validator->fails()){
            return ['errror'=>true,'errorMessage'=>'Fields not meeting the rqguired format','fields'=>$validator->errors()];
        }

        return Tblevent::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\Tblevent  $tblevent
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tblevent = Tblevent::find($id);
        if(!$tblevent){
            return ['error'=>true,'errorMessage'=>"Event with id = $id does not exist"];
        }
        return $tblevent;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\model\Tblevent  $tblevent
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return $this->show($id);
    }

    /**
     * @OA\Put(
     *      path="/events",
     *      operationId="updateEvent",
     *      tags={"Events"},
     *      summary="update event",
     *      description="Returns event data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Tblevent")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Tblevent")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\Tblevent  $tblevent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $tblevent = $this->show($id);

        $validator = Validator::make($request->all(),[
            'title'=>['required'],
            'description'=>['required'],
            'start_date'=>['required'],
            'end_date'=>['required'],
            'id'=>'required',
        ]);

        if($validator->fails()){
            return ['errror'=>true,'errorMessage'=>'Fields not meeting the rqguired format','fields'=>$validator->errors()];
        }

        return Tblevent::update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\Tblevent  $tblevent
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tblevent = $this->show($id);

        return ["deleted" => $tblevent->delete()];
    }
}
