<?php

namespace App\Http\Controllers;

use App\model\Tblrole;
use Illuminate\Http\Request;

class TblroleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tblrole  $tblrole
     * @return \Illuminate\Http\Response
     */
    public function show(Tblrole $tblrole)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tblrole  $tblrole
     * @return \Illuminate\Http\Response
     */
    public function edit(Tblrole $tblrole)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tblrole  $tblrole
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tblrole $tblrole)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tblrole  $tblrole
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tblrole $tblrole)
    {
        //
    }
}
