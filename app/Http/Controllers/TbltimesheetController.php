<?php

namespace App\Http\Controllers;

use App\model\Tbltimesheet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TbltimesheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $timesheets = Tbltimesheet::all();
        foreach ($timesheets as $value) {
            $value->staff;
        }

        return $timesheets;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * @OA\Post(
     *      path="/timesheet",
     *      operationId="createTimesheet",
     *      tags={"timesheet"},
     *      summary="Create timesheet",
     *      description="Returns timesheet data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Tbltimesheet")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Tbltimesheet")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(),[
            'company_id'=>['required'],
            'staff_id'=>['required'],
            'start_time'=>['required'],
            'end_time'=>['required'],
            'start_date'=>['required'],
            'end_date'=>['required'],
            'break_start_at'=>['required'],
            'break_end_at'=>['required'],
        ]);

        if($validator->fails()){
            return ['error'=>true, 'errorMessage'=>"Data you have provided is has some fields failing to pass validation",
                    "fields"=>$validator->errors()];
        }

        return Tbltimesheet::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\Tbltimesheet  $tbltimesheet
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tbltimesheet = Tbltimesheet::find($id);
        if(!$tbltimesheet){
            return ['error'=>true,'errorMessage'=>"Timesheet with id = $id does not exist"];
        }
        $tbltimesheet->company;
        $tbltimesheet->staff;
        return $tbltimesheet;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\model\Tbltimesheet  $tbltimesheet
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return $this->show($id);
    }

    /**
     * @OA\Put(
     *      path="/timesheet",
     *      operationId="updateTimesheet",
     *      tags={"timesheet"},
     *      summary="Update timesheet",
     *      description="Returns timesheet data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Tbltimesheet")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Tbltimesheet")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\Tbltimesheet  $tbltimesheet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $tbltimesheet = $this->show($id);
        $validator = Validator::make($request->all(),[
            'company_id'=>['required'],
            'staff_id'=>['required'],
            'start_time'=>['required'],
            'end_time'=>['required'],
            'start_date'=>['required'],
            'end_date'=>['required'],
            'break_start_at'=>['required'],
            'break_end_at'=>['required'],
            'id'=>'required'
        ]);

        if($validator->fails()){
            return ['error'=>true, 'errorMessage'=>"Data you have provided is has some fields failing to pass validation",
                    "fields"=>$validator->errors()];
        }

        return Tbltimesheet::update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\Tbltimesheet  $tbltimesheet
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tbltimesheet = $this->show($id);
        return ['delete'=>$tbltimesheet->delete()];
    }
}
