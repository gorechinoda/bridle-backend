<?php

namespace App\Http\Controllers\location;

use App\model\location\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cities = City::all();
        foreach ($cities as $value) {
            # code...
            $value->province;
        }
        return $cities;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            "name"=>"required",
            "province_id"=>"required"
        ]);

        return City::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\location\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        //
        $city->province;
        return $city;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\model\location\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        //
        return $city;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\location\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        //
        $city->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\location\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        //
        return $city->delete();
    }
}
