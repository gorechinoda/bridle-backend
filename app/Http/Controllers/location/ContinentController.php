<?php

namespace App\Http\Controllers\location;

use App\model\location\Continent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContinentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $continent = Continent::all();
        foreach ($continent as $value) {
            # code...
            $value->countries;
        }
        return $continent;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            "name"=>"required",
        ]);

        return Continent::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\location\Continent  $continent
     * @return \Illuminate\Http\Response
     */
    public function show(Continent $continent)
    {
        //
        return $continent;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\model\location\Continent  $continent
     * @return \Illuminate\Http\Response
     */
    public function edit(Continent $continent)
    {
        //
        return $continent;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\location\Continent  $continent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Continent $continent)
    {
        //
        $continent->update($request->all());
        return $continent;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\location\Continent  $continent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Continent $continent)
    {
        //
        $continent->delete();
    }
}
