<?php

namespace App\Http\Controllers\location;

use App\model\location\Province;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProvinceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $provinces = Province::all();
        foreach ($provinces as $value) {
            # code...
            $value->cities;
            $value->country;
        }
        return $provinces;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            "name"=>"required",
            "country_id"=>"required",
        ]);

        return Province::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\location\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function show(Province $province)
    {
        //
        $province->coutnry;
        $province->cities;
        return $province;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\model\location\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function edit(Province $province)
    {
        //
        return $province;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\location\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Province $province)
    {
        //
        $province->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\location\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function destroy(Province $province)
    {
        //
        $request->delete();
    }
}
