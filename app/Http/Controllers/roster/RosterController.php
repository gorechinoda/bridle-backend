<?php

namespace App\Http\Controllers\roster;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\model\roster\Tblroster;

class RosterController extends Controller
{
    //
    public function index(){
        return Tblroster::all();
    }

    public function create(){

    }

     /**
     * @OA\Post(
     *      path="/roster",
     *      operationId="createRoster",
     *      tags={"Roster"},
     *      summary="Create Roster",
     *      description="Returns Roster data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Tbltimesheet")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Tbltimesheet")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function store(Request $request){
        $validator = Validator::create($request->all(),[
            'client_d'=>['required'],
            'staff_id'=>['required'],
            'support_area_id'=>'required',
            'support_service_id'=>'required',
            'end_date'=>['required'],
            'start_date'=>['required'],
        ]);

        if($validator->fails()){
            return ['error'=>true,'errorMessage'=>"some fields failed validation requirement","field"=>$validator->errors()];
        }

        return Tblroster::create($request->all());
    }

    public function show($id){
        $tblroster = Tblroster::find($id);
        if(!$tblroster){
            return ["error"=>true,"errorMessage"=>"Roster with id = $id does not exist!"];
        }

        $tblroster->company;
        $tblroster->staff;
        $tblroster->client;
        $tblroster->service;
        $tblroster->financialUsage;

        return $tblroster;
    }

    public function edit($id){
        return $this->show($id);
    }


    /**
     * @OA\Put(
     *      path="/roster",
     *      operationId="updateRoster",
     *      tags={"Roster"},
     *      summary="Update Roster",
     *      description="Returns Roster data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Tblroster")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Tblroster")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function update(Request $request,$id){
        $this->show($id);
        $validator = Validator::create($request->all(),[
            'client_d'=>['required'],
            'staff_id'=>['required'],
            'support_area_id'=>'required',
            'support_service_id'=>'required',
            'end_date'=>['required'],
            'start_date'=>['required'],
            "id"=>"required"
        ]);

        if($validator->fails()){
            return ['error'=>true,'errorMessage'=>"some fields failed validation requirement","field"=>$validator->errors()];
        }

        return Tblroster::update($request->all());
    }


}
