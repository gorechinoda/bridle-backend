<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @OA\Schema(
 *     title="User",
 *     description="User model",
 *     @OA\Xml(
 *         name="User"
 *     )
 * )
 */
class User extends Authenticatable
{
    use Notifiable,HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','surname','date_of_birth','id','username',
        "mobile_number","gender_id","postcode","address","city_id","status"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Name",
     *      description="Name of the new User",
     *      example="Milton"
     * )
     *
     * @var string
     */
    private $name;

    /**
     * @OA\Property(
     *      title="Surname",
     *      description="Surname of the new User",
     *      example="Gore"
     * )
     *
     * @var string
     */
    private $surname;

    /**
     * @OA\Property(
     *      title="Email",
     *      description="Email of the new User",
     *      example="gorechinoda@gmail.com"
     * )
     *
     * @var string
     */
    private $email;

    /**
     * @OA\Property(
     *      title="Password",
     *      description="Password of the new User",
     *      example="12345678"
     * )
     *
     * @var string
     */
    private $password;

    /**
     * @OA\Property(
     *      title="Username",
     *      description="Username of the new User",
     *      example="milaz"
     * )
     *
     * @var string
     */
    private $username;

    /**
     * @OA\Property(
     *      title="Mobile Number",
     *      description="Mobile number of the new User",
     *      example="0774226683"
     * )
     *
     * @var string
     */
    private $mobile_number;

    /**
     * @OA\Property(
     *      title="Date of birth",
     *      description="Date of birth for the new User",
     *      example="1995-05-28"
     * )
     *
     * @var date
     */
    private $date_of_birth;

}
