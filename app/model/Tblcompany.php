<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     title="Company",
 *     description="Company model",
 *     @OA\Xml(
 *         name="Company"
 *     )
 * )
 */
class Tblcompany extends Model
{
    //
    protected $fillable = [
        'id','status','company_email','company_name','company_mobile','employee_range_id',
        'date_created','date_updated'
    ];

    public function employee_range(){
        return $this->belongsTo('App\model\TblemplyeeRange');
    }

    public function staff(){
        return $this->hasMany("App\model\TblcompanyStaff", 'company_id', 'id');
    }

    public function clients(){
        return $this->hasMany("App\model\TblcompanyClient", 'company_id', 'id');
    }

    public function licenses(){
        return $this->hasMany("App\model\TblcompanyLicense", 'company_id', 'id');
    }

    public function timesheets()
    {
        return $this->hasMany('App\model\Tbltimesheet', 'company_id', 'id');
    }

    public function events(){
        return $this->hasMany('App\model\Tblevent', 'company_id', 'id');
    }

    /**
     * @OA\Property()
     *
     * @var integer
     */
    private $id;

    /**
     * @OA\Property()
     *
     * @var bool
     */
    private $status;

    /**
     * @OA\Property(
     *      title="email",
     *      example="gorechinoda@gmail.com"
     * )
     *
     * @var string
     */
    private $company_email;

    /**
     * @OA\Property()
     *
     * @var string
     */
    private $company_name;

    /**
     * @OA\Property()
     *
     * @var string
     */
    private $company_mobile;

    /**
     * @OA\Property()
     *
     * @var integer
     */
    private $employee_range_id;

    /**
     * @OA\Property()
     *
     * @var \App\model\TblemplyeeRange
     */
    private $employee_range;

    //
    //  * OA\Property()
    //  *
    //  * var \App\model\TblcompanyStaff[]
    //  */
    // private $staff;

    // /**
    //  * OA\Property()
    //  *
    //  * var \App\model\TblcompanyClient[]
    //  */
    // private $clients;

    // /**
    //  * OA\Property()
    //  *
    //  * var \App\model\TblcompanyLicense[]
    //  */
    // private $licenses;


    //
    //  * OA\Property()
    //  *
    //  * var \App\model\Tbltimesheet[]
    //
    // private $timesheets;

}
