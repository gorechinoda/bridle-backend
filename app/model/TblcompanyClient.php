<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="CompanyClient",
 *      @OA\Xml(
 *          name="CompanyClient",
 *      )
 * )
 */
class TblcompanyClient extends Model
{
    //
    protected $fillable = [
        'id','company_id','users_id','ndis'
    ];

    public function user(){
        return $this->belongsTo('App\User','users_id');
    }

    public function company(){
        return $this->belongsTo('App\model\Tblcompany','company_id');
    }

    public function contacts()
    {
        return $this->hasMany('App\model\client\contact\TbladditionalContacts', 'client_id', 'id');
    }

    public function plans()
    {
        return $this->hasMany('App\model\plan\TblclientPlans', 'client_id', 'id');
    }

    public function consents()
    {
        return $this->hasMany('App\model\client\consent\TblclientConsent', 'client_id', 'id');
    }

    public function financial()
    {
        return $this->hasMany('App\model\client\finance\TblclientFinancial', 'client_id', 'id');
    }

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID",
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Company ID",
     *      example="1",
     *      description="Company ID",
     * )
     * 
     * @var integer
     */
    private $company_id;

    /**
     * @OA\Property(
     *      title="User ID",
     *      example="1",
     *      description="USer ID",
     * )
     * 
     * @var integer
     */
    private $users_id;

    /**
     * @OA\Property(
     *      title="NDIS",
     *      example="Niga",
     *      description="NDIS",
     * )
     * 
     * @var string
     */
    private $ndis;

    /**
     * @OA\Property(
     *      title="User",
     *      example="1",
     *      description="ID",
     * )
     * 
     * @var \App\User
     */
    private $user;

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID",
     * )
     * 
     * @var \App\model\Tblcompany
     */
    private $company;

    /**
     * @OA\Property(
     * )
     * 
     * @var \App\model\client\contact\TbladditionalContacts[]
     */
    private $contacts;


    /**
     * @OA\Property(
     * )
     * 
     * @var \App\model\plan\TblclientPlans[]
     */
    private $plans;

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID",
     * )
     * 
     * @var \App\model\client\consent\TblclientConsent[]
     */
    private $consents;

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID",
     * )
     * 
     * @var \App\model\client\finance\TblclientFinancial[]
     */
    private $financial;
}
