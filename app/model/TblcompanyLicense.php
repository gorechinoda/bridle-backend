<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="CompanyLicence",
 *      description="Company Licence",
 *      @OA\Xml(
 *          name="CompanyLicence",
 *      )
 * )
 */
class TblcompanyLicense extends Model
{
    //
    protected $fillable = [
        'id', 'company_id', 'description', 'date_writen', 'active'
    ];

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID",
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Company ID",
     *      example="1",
     *      description="Company ID",
     * )
     * 
     * @var integer
     */
    private $company_id;

    /**
     * @OA\Property(
     *      title="Description",
     *      example="This is so fun",
     *      description="Description",
     * )
     * 
     * @var string
     */
    private $description;

    /**
     * @OA\Property(
     *      title="Date written",
     *      description="Date written",
     * )
     * 
     * @var date
     */
    private $date_writen;

    /**
     * @OA\Property(
     *      title="Active",
     *      example="true",
     *      description="Active",
     * )
     * 
     * @var bool
     */
    private $active;

}
