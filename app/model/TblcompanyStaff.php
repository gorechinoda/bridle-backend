<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="CompanyStaff",
 *      @OA\Xml(
 *          name="CompanyStaff",
 *      )
 * )
 */

class TblcompanyStaff extends Model
{
    //
    protected $fillable = [
        'id','role_id','company_id','users_id'
    ];

    public function user(){
        return $this->belongsTo('App\User','users_id');
    }

    public function company(){
        return $this->belongsTo('App\model\Tblcompany','company_id');
    }

    public function role(){
        return $this->belongsTo('App\model\Tblrole','role_id');
    }

    public function contacts()
    {
        return $this->hasMany('App\model\staff\TblstaffContact', 'staff_id', 'id');
    }

    public function professionals()
    {
        return $this->hasMany('App\model\staff\TblstaffProfessional', 'staff_id', 'id');
    }

    public function education()
    {
        return $this->hasMany('App\model\staff\TblstaffEducation', 'staff_id', 'id');
    }

    public function expirience()
    {
        return $this->hasMany('App\model\staff\TblstaffExpirience', 'staff_id', 'id');
    }

    public function documents()
    {
        return $this->hasMany('App\model\staff\TblstaffDocument', 'staff_id', 'id');
    }
    
    public function employmentInformation()
    {
        return $this->hasOne('App\model\staff\TblstaffEmploymentInformation', 'staff_id', 'id');
    }

    public function bankingInformation()
    {
        return $this->hasMany('App\model\staff\TblstaffBankingInformation', 'staff_id', 'id');
    }

    public function references()
    {
        return $this->hasMany('App\model\staff\TblstaffReference', 'staff_id', 'id');
    }

    public function emegencyContacts()
    {
        return $this->hasMany('App\model\staff\TblstaffEmegencyContact', 'foreign_key', 'local_key');
    }

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID",
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Company ID",
     *      example="1",
     *      description="Company ID",
     * )
     * 
     * @var integer
     */
    private $company_id;

    /**
     * @OA\Property(
     *      title="User ID",
     *      example="1",
     *      description="USer ID",
     * )
     * 
     * @var integer
     */
    private $users_id;

    /**
     * @OA\Property(
     *      title="Role ID",
     *      example="1",
     *      description="Role ID",
     * )
     * 
     * @var integer
     */
    private $role_id;

    //////////////////////////////////////////////////////////////////////////////////

    /**
     * @OA\Property()
     * 
     * @var \App\User
     */
    private $user;

     /**
     * @OA\Property()
     * 
     * @var \App\model\Tblcompany
     */
    private $company;

     /**
     * @OA\Property()
     * 
     * @var \App\model\Tblrole
     */
    private $role;

     /**
     * @OA\Property()
     * 
     * @var \App\model\staff\TblstaffContact[]
     */
    private $contacts;

     /**
     * @OA\Property()
     * 
     * @var \App\model\staff\TblstaffProfessional[]
     */
    private $professionals;

     /**
     * @OA\Property()
     * 
     * @var \App\model\staff\TblstaffEducation[]
     */
    private $education;

     /**
     * @OA\Property()
     * 
     * @var \App\model\staff\TblstaffExpirience[]
     */
    private $expirience;

     /**
     * @OA\Property()
     * 
     * @var \App\model\staff\TblstaffDocument[]
     */
    private $documents;

     /**
     * @OA\Property()
     * 
     * @var \App\model\staff\TblstaffEmploymentInformation
     */
    private $employmentInformation;

     /**
     * @OA\Property()
     * 
     * @var \App\model\staff\TblstaffBankingInformation[]
     */
    private $bankingInformation;

     /**
     * @OA\Property()
     * 
     * @var \App\model\staff\TblstaffReference[]
     */
    private $references;

     /**
     * @OA\Property()
     * 
     * @var \App\model\staff\TblstaffEmegencyContact[]
     */
    private $emegencyContacts;
}
