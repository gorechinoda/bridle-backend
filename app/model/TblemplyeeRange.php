<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="EmployeeRange",
 *      @OA\Xml(
 *          name="EmployeeRange",
 *      ),
 * )
 */
class TblemplyeeRange extends Model
{
    //
    protected $fillable = [
        'id', 'name'
    ];

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID",
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="name",
     *      example="Milton",
     *      description="Name"
     * )
     * 
     * @var string
     */
    public $name;
}
