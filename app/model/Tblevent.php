<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="Event",
 *      description="Event model",
 *      @OA\Xml(
 *          name="Event"
 *      )
 * )
 */
class Tblevent extends Model
{
    //
    protected $fillable = [
        'id', 'title', 'description', 'start_date', 'end_date',"company_id"
    ];

    /**
     * @OA\Property()
     *
     * @var integer
     */
    protected $id;

    /**
     * @OA\Property()
     *
     * @var string
     */
    private $title;

    /**
     * @OA\Property(
     *      title="Description",
     *      description="Description of the event",
     * )
     *
     * @var string
     */
    private $description;

    /**
     * @OA\Property(
     *      title="Start date",
     *      description="Starting date of the event",
     * )
     *
     * @var dateTime
     */
    private $start_date;

    /**
     * @OA\Property(
     *      title="End Date",
     *      description="Ending date of the event",
     * )
     *
     * @var dateTime
     */
    private $end_date;

    /**
     * @OA\Property(
     *      title="company_id",
     *      description="Id to refference company",
     * )
     *
     * @var integer
     */
    private $company_id;

}
