<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="Timesheet",
 *      @OA\Xml(
 *          name="Timesheet",
 *      )
 * )
 */
class Tbltimesheet extends Model
{
    //
    protected $fillable = [
        'id', 'company_id', 'staff_id', 'start_time', 'end_time', 'start_date', 
        'end_date', 'break_start_at', 'break_end_at'
    ];

    public function company()
    {
        return $this->belongsTo('App\model\Tblcompany', 'company_id', 'id');
    }

    public function staff()
    {
        return $this->belongsTo('App\model\TblcompanyStaff', 'staff_id', 'id');
    }

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Company ID",
     *      example="1",
     *      description="Company ID"
     * )
     * 
     * @var integer
     */
    private $company_id;

    /**
     * @OA\Property(
     *      title="Satrt time",
     *      description="Start time"
     * )
     * 
     * @var datetime
     */
    private $start_time;

    /**
     * @OA\Property(
     *      title="End time",
     *      description="End time"
     * )
     * 
     * @var datetime
     */
    private $end_time;

    /**
     * @OA\Property(
     *      title="Start date",
     *      description="Start Date"
     * )
     * 
     * @var date
     */
    private $start_date;

    /**
     * @OA\Property(
     *      title="End date",
     *      description="End time"
     * )
     * 
     * @var date
     */
    private $end_date;

    /**
     * @OA\Property(
     *      title="Break time",
     *      description="Break time start at"
     * )
     * 
     * @var datetime
     */
    private $break_start_at;

    /**
     * @OA\Property(
     *      title="Break time",
     *      description="Break time ends at"
     * )
     * 
     * @var datetime
     */
    private $break_end_at;

    /**
     * @OA\Property(
     *      title="Company ID",
     *      example="1",
     *      description="Company ID"
     * )
     * 
     * @var \App\model\Tblcompany
     */
    private $company;

    /**
     * @OA\Property(
     *      title="Company ID",
     *      example="1",
     *      description="Company ID"
     * )
     * 
     * @var \App\model\TblcompanyStaff
     */
    private $staff;
}
