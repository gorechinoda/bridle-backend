<?php

namespace App\model\client\consent;

use Illuminate\Database\Eloquent\Model;


/**
 * @OA\Schema(
 *      title="ClientConsent",
 *      @OA\Xml(
 *          name="ClientConsent"
 *      )
 * )
 */
class TblclientConsent extends Model
{
    //
    protected $fillable = [
        'id','consent_type_id','client','person','expiry_date',
    ];

    
    public function consentType()
    {
        return $this->belongsTo('App\model\client\TblconsentType', 'consent_type_id', 'id');
    }

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Client ID",
     *      example="1",
     *      description="Client id"
     * )
     * 
     * @var integer
     */
    public $client_id;

    /**
     * @OA\Property(
     *      title="Person",
     *      example="Hie",
     *      description="Person"
     * )
     * 
     * @var string
     */
    public $person;

    /**
     * @OA\Property(
     *      title="Expiry Date",
     *      example="2020-06-30",
     *      description="Expiry Date"
     * )
     * 
     * @var date
     */
    public $expiry_date;

    /**
     * @OA\Property(
     *      title="Consent Type Id",
     *      example="1",
     *      description="Consent Type id required when creating and updating Client Consent To link with Consent Type"
     * )
     * 
     * @var integer
     */
    public $consent_type_id;
   
    /**
     * @OA\Property(
     *      title="Consent Type",
     *      description="Consent type i linked to here by ID, not required when creating Client consent."
     * )
     * 
     * @var \App\model\client\consent\TblconsentType
     */
    private $consentType;
}
