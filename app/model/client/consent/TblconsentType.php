<?php

namespace App\model\client\consent;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="ConsentType",
 *      @OA\Xml(
 *          name="ConsentType"
 *      )
 * )
 */
class TblconsentType extends Model
{
    //
    protected $fillable = [
        'id', 'name',
    ];

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="name",
     *      example="Milton",
     *      description="Name"
     * )
     * 
     * @var string
     */
    public $name;
}
