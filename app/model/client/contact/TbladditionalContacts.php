<?php

namespace App\model\client\contact;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="AdditionalContact",
 *      @OA\Xml(
 *          name="AdditionalContact",
 *      )
 * )
 */
class TbladditionalContacts extends Model
{
    //
    protected $fillable = [
        'id','contact_type_id', 'name', 'home_phone', 'work_phone', 'mobile_phone', 'comment', 'client_id', 'postal_address',
        'email', 'relationship_contact_method',
    ];

    public function contactType()
    {
        return $this->belongsTo('App\model\client\'TblcontactType', 'contact_type_id', 'id');
    }

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Contact Type ID",
     *      example="1",
     *      description="Contact Type ID"
     * )
     * 
     * @var integer
     */
    private $contact_type_id;

    /**
     * @OA\Property(
     *      title="Name",
     *      example="Milton",
     *      description="Name"
     * )
     * 
     * @var string
     */
    private $name;

    /**
     * @OA\Property(
     *      title="Home Phone",
     *      example="0787878787",
     *      description="Home Phone"
     * )
     * 
     * @var string
     */
    private $home_phone;

    /**
     * @OA\Property(
     *      title="Work Phone",
     *      example="078989747",
     *      description="Work phone"
     * )
     * 
     * @var string
     */
    private $work_phone;

    /**
     * @OA\Property(
     *      title="Mobile Phone",
     *      example="0717347832",
     *      description="Mobile Phone"
     * )
     * 
     * @var string
     */
    private $mobile_phone;

    /**
     * @OA\Property(
     *      title="Comment",
     *      example="wow how is he doing",
     *      description="Comment"
     * )
     * 
     * @var string
     */
    private $comment;

    /**
     * @OA\Property(
     *      title="Client ID",
     *      example="1",
     *      description="Client ID"
     * )
     * 
     * @var integer
     */
    private $client_id;

    /**
     * @OA\Property(
     *      title="Postal Address",
     *      example="chinhoyi Zimbabwe",
     *      description="Postal Address"
     * )
     * 
     * @var string
     */
    private $postal_address;

    /**
     * @OA\Property(
     *      title="Email",
     *      example="test@bridle.com",
     *      description="Email"
     * )
     * 
     * @var string
     */
    private $email;

    /**
     * @OA\Property(
     *      title="Relationship Contact Method",
     *      example="I dont get this one bro",
     *      description="Relationship Contact Method"
     * )
     * 
     * @var string
     */
    private $relationship_contact_method;

    /**
     * @OA\Property(
     *      title="Contatct type",
     *      description="Contact type"
     * )
     * 
     * @var \App\model\client\contact\TblcontactType
     */
    private $contactType;

}
