<?php

namespace App\model\client\contact;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="ContactType",
 *      @OA\Xml(
 *          name="ContactType",
 *      )
 * )
 */
class TblcontactType extends Model
{
    //
    protected $fillable = [
        'id','name'
    ];

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Name",
     *      example="Tesla",
     *      description="Name"
     * )
     * 
     * @var string
     */
    private $name;
}
