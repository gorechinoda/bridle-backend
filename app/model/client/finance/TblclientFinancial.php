<?php

namespace App\model\client\finance;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="ClientFinancial",
 *      @OA\Xml(
 *          name="ClientFinancial",
 *      )
 * )
 */
class TblclientFinancial extends Model
{
    //
    protected $fillable =[
        'id','client_id','name','description','amount','status',
    ];

    public function client()
    {
        return $this->belongsTo('App\modle\TblcompanyClient', 'client_id', 'id');
    }

    public function financialSupport()
    {
        return $this->hasMany('App\model\client\finance\TblclientFinancialSupport', 'financial_id', 'id');
    }

    public function financialUsage()
    {
        return $this->hasMany('App\model\clent\finance\TblclientFinancialUsage', 'financial_id', 'id');
    }

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Client ID",
     *      example="1",
     *      description="Client ID"
     * )
     * 
     * @var integer
     */
    private $client_id;

    /**
     * @OA\Property(
     *      title="Name",
     *      example="Midza",
     *      description="Name"
     * )
     * 
     * @var string
     */
    private $name;

    /**
     * @OA\Property(
     *      title="Description",
     *      example="This is financial",
     *      description="Description"
     * )
     * 
     * @var string
     */
    private $description;

    /**
     * @OA\Property(
     *      title="Amount",
     *      example="89.08",
     *      description="Amount"
     * )
     * 
     * @var double
     */
    private $amount;

    /**
     * @OA\Property(
     *      title="Status",
     *      example="true",
     *      description="Status"
     * )
     * 
     * @var bool
     */
    private $status;

    /**
     * @OA\Property(
     *      title="Client",
     *      description="Client"
     * )
     * 
     * @var \App\model\TblcompanyClient
     */
    private $client;

    /**
     * @OA\Property(
     *      title="Financial Support",
     *      description="Financial Support"
     * )
     * 
     * @var \App\model\client\finance\TblclientFinancialSupport[]
     */
    private $financialSupport;

    /**
     * @OA\Property(
     *      title="Financial Usage",
     *      description="Financial Usage"
     * )
     * 
     * @var \App\model\client\finance\TblclientFinancialUsage[]
     */
    private $financialUsage;


}
