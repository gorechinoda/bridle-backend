<?php

namespace App\model\client\finance;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="ClientFinancialSupport",
 *      @OA\Xml(
 *          name="ClientFinancialSupport",
 *      )
 * )
 */
class TblclientFinancialSupport extends Model
{
    //
    protected $fillable = [
        'id','financial_id','support_service_id',
    ];

    public function supportService()
    {
        return $this->belongsTo('App\model\support\TblsupportService', 'support_service_id', 'id');
    }

    public function financial()
    {
        return $this->belongsTo('App\model\client\finance\TblclientFinancial', 'financial_id', 'id');
    }

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Financial ID",
     *      example="1",
     *      description="Financial ID"
     * )
     * 
     * @var integer
     */
    private $financial_id;

    /**
     * @OA\Property(
     *      title="Support service ID",
     *      example="1",
     *      description="Support service ID"
     * )
     * 
     * @var integer
     */
    private $support_service_id;

    /**
     * @OA\Property(
     *      title="Support service",
     *      description="Support service"
     * )
     * 
     * @var \App\model\support\TblsupportService
     */
    private $supportService;

     /**
     * @OA\Property(
     *      title="Client financial",
     *      description="Client financial"
     * )
     * 
     * @var \App\model\client\finance\TblclientFinancial
     */
    private $financial;

}
