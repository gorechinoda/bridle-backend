<?php

namespace App\model\client\finance;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="ClientFinancialUsage",
 *      @OA\Xml(
 *          name="ClientFinancialUsage",
 *      )
 * )
 */
class TblclientFinancialUsage extends Model
{
    //
    protected $fillable = [
        'id','financial_id','roster_id','amount',
    ];

    public function financial()
    {
        return $this->belongsTo('App\model\client\finance\TblclientFinancial', 'financial_id', 'id');
    }

    public function roster()
    {
        return $this->belongsTo('App\model\roster\Tblroster', 'roster_id', 'id');
    }

     /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

     /**
     * @OA\Property(
     *      title="Financial ID",
     *      example="1",
     *      description="Financial ID"
     * )
     * 
     * @var integer
     */
    private $financial_id;

     /**
     * @OA\Property(
     *      title="Roster ID",
     *      example="1",
     *      description="Roster ID"
     * )
     * 
     * @var integer
     */
    private $roster_id;

     /**
     * @OA\Property(
     *      title="Amount",
     *      example="23.08",
     *      description="Amount"
     * )
     * 
     * @var double
     */
    private $amount;

     /**
     * @OA\Property(
     *      title="Financial",
     *      description="Financial"
     * )
     * 
     * @var \App\model\client\finance\TblclientFinancial
     */
    private $financial;

     /**
     * @OA\Property(
     *      title="Roster",
     *      description="Roster"
     * )
     * 
     * @var \App\model\roster\Tblroster
     */
    private $roster;

}