<?php

namespace App\model\location;

use Illuminate\Database\Eloquent\Model;
/**
 * @OA\Schema(
 *      title="City",
 *      @OA\Xml(
 *          name="City",
 *      )
 * )
 */
class City extends Model
{
    //
    protected $fillable = [
        "id","name","province_id","created_at","updated_at",
    ];

    public function province(){
        return $this->belongsTo("App\model\location\Province");
    }
}
