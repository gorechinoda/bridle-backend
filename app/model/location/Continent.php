<?php

namespace App\model\location;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="Continent",
 *      @OA\Xml(
 *          name="Continent",
 *      )
 * )
 */
class Continent extends Model
{
    //
    protected $fillable = [
        "id","name",
    ];

    public function countries(){
        return $this->hasMany("App\model\location\Coutnry");
    }
}
