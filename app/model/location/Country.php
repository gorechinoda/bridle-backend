<?php

namespace App\model\location;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="Country",
 *      @OA\Xml(
 *          name="Country",
 *      )
 * )
 */
class Country extends Model
{
    //
    protected $fillable = [
        "id","name","continent_id",
    ];

    public function continent(){
        return $this->belongsTo('App\model\location\Continent', 'continent_id', 'id');
    }

    public function provinces(){
        return $this->hasMany('App\model\location\Province', 'country_id', 'id');
    }
}
