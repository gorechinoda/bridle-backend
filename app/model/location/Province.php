<?php

namespace App\model\location;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="Province",
 *      @OA\Xml(
 *          name="Province",
 *      )
 * )
 */
class Province extends Model
{
    //
    protected $fillabe = [
        "id","name","country_id",
    ];

    public function country(){
        return $this->belongsTo('App\model\location\Country', 'country_id', 'id');
    }

    public function cities()
    {
        return $this->hasMany('App\model\location\City', 'province_id', 'id');
    }
    
}
