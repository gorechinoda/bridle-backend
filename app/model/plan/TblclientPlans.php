<?php

namespace App\model\plan;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="ClientPlan",
 *      @OA\Xml(
 *          name="ClientPlan",
 *      )
 * )
 */
class TblclientPlans extends Model
{
    //
    protected $fillable = [
        'plan_type_id','ndis_number','start_date','end_date', 'id', 'client_id'
    ];

    public function planType()
    {
        return $this->belongsTo('App\model\plan\TblplanType', 'plan_type_id', 'id');
    }

    
    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="plan type ID",
     *      example="1",
     *      description="plan type ID"
     * )
     * 
     * @var integer
     */
    private $plan_type_id;

    /**
     * @OA\Property(
     *      title="NDIS number",
     *      example="sfsjhfgksjfh",
     *      description="NDIS number"
     * )
     * 
     * @var string
     */
    private $ndis_number;

    /**
     * @OA\Property(
     *      title="Start date",
     *      example="2020-06-23",
     *      description="Start date"
     * )
     * 
     * @var date
     */
    private $start_date;

    /**
     * @OA\Property(
     *      title="End Date",
     *      example="2021-05-25",
     *      description="End Date"
     * )
     * 
     * @var date
     */
    private $end_date;

    /**
     * @OA\Property(
     *      title="Client ID",
     *      example="1",
     *      description="Client ID"
     * )
     * 
     * @var integer
     */
    private $client_id;

    /**
     * @OA\Property(
     *      title="Financial Usage",
     *      description="Financial Usage"
     * )
     * 
     * @var \App\model\plan\TblplanType
     */
    private $planType;
    
}
