<?php

namespace App\model\plan;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="PlanType",
 *      @OA\Xml(
 *          name="PlanType",
 *      )
 * )
 */
class TblplanType extends Model
{
    //
    protected $fillable = [
        'id', 'name',
    ];

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Name",
     *      example="Testing",
     *      description="Name"
     * )
     * 
     * @var string
     */
    private $name;
}
