<?php

namespace App\model\roster;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="Roster",
 *      @OA\Xml(
 *          name="Roster",
 *      )
 * )
 */
class Tblroster extends Model
{
    //
    protected $fillable = [
        'id','company_id','staff_id','client_id','service_id','status','sdt','edt',
    ];

    public function company()
    {
        return $this->belongsTo('App\Tblcompany', 'company_id', 'id');
    }

    public function staff()
    {
        return $this->belongsTo('App\TblcompanyStaff', 'staff_id', 'id');
    }

    public function client()
    {
        return $this->belongsTo('App\TblcompanrClient', 'client_id', 'id');
    }

    public function service()
    {
        return $this->belongsTo('App\support\TblsupportService', 'service_id', 'id');
    }
    
    public function financialUsage()
    {
        return $this->hasMany('App\model\client\finance\TblclientFinancialUsage', 'roster_id', 'id');
    }

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Company ID",
     *      example="1",
     *      description="Company ID"
     * )
     * 
     * @var integer
     */
    private $company_id;

    /**
     * @OA\Property(
     *      title="Staff ID",
     *      example="1",
     *      description="Staff ID"
     * )
     * 
     * @var integer
     */
    private $staff_id;

    /**
     * @OA\Property(
     *      title="Client ID",
     *      example="1",
     *      description="Client ID"
     * )
     * 
     * @var integer
     */
    private $client_id;

    /**
     * @OA\Property(
     *      title="Service ID",
     *      example="1",
     *      description="Service ID"
     * )
     * 
     * @var integer
     */
    private $service_id;

    /**
     * @OA\Property(
     *      title="Status",
     *      example="1",
     *      description="Status"
     * )
     * 
     * @var bool
     */
    private $status;

    /**
     * @OA\Property(
     *      title="Start Date",
     *      description="Start date"
     * )
     * 
     * @var dateTime
     */
    private $std;

    /**
     * @OA\Property(
     *      title="Start Date",
     *      description="Start date"
     * )
     * 
     * @var dateTime
     */
    private $edt;

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var \App\model\Tblcompany
     */
    private $company;

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var \App\model\TblcompanyStaff
     */
    private $staff;

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var \App\model\TblcompanyClient
     */
    private $client;

    /**
     * @OA\Property(
     *      title="Suapport Service",
     * )
     * 
     * @var \App\model\support\TblsupportService
     */
    private $service;

     /**
     * @OA\Property(
     *      title="FinancialUsage",
     *      description="Financial Usage"
     * )
     * 
     * @var \App\model\client\finance\TblclientFinancialUsage
     */
    private $financialUsage;
}
