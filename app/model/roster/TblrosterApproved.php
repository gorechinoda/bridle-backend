<?php

namespace App\model\roster;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="RosterApproved",
 *      @OA\Xml(
 *          name="RosterApproved",
 *      )
 * )
 */
class TblrosterApproved extends Model
{
    //
    protected $fillable = [
        'id','roster_id','approved_by'
    ];

    public function approvedBy()
    {
        return $this->belongsTo('App\model\TblcompanyStaff', 'approved_by', 'id');
    }
    
    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Roster ID",
     *      example="1",
     *      description="Roster Approved"
     * )
     * 
     * @var integer
     */
    private $roster_id;

    /**
     * @OA\Property(
     *      title="Approved By",
     *      example="1",
     *      description="User who approves the roster"
     * )
     * 
     * @var integer
     */
    private $approved_by;

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var \App\model\TblcompanyStaff
     */
    private $approvedBy;
}
