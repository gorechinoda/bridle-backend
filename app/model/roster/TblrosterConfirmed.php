<?php

namespace App\model\roster;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="RosterConfirmed",
 *      @OA\Xml(
 *          name="RosterConfirmed",
 *      )
 * )
 */
class TblrosterConfirmed extends Model
{
    //
    protected $fillable = [
        'id','roster_id','confirmed_by',
    ];
    

    public function confirmedBy()
    {
        return $this->belongsTo('App\model\TblcompanyStaff', 'confirmed_by', 'id');
    }
    
    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Roster ID",
     *      example="1",
     *      description="Roster Approved"
     * )
     * 
     * @var integer
     */
    private $roster_id;

    /**
     * @OA\Property(
     *      title="Confirmed By",
     *      example="1",
     *      description="User who confirmed the roster"
     * )
     * 
     * @var integer
     */
    private $confirmed_by;

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var \App\model\TblcompanyStaff
     */
    private $confirmedBy;
}
