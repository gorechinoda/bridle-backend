<?php

namespace App\model\staff;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="StaffBankingInformation",
 *      @OA\Xml(
 *          name="StaffBankingInformation",
 *      )
 * )
 */
class TblstaffBankingInformation extends Model
{
    //
    protected $fillable = [
        'id','bank_name','account_name','account_number','staff_id',
    ];

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Bank name",
     *      example="ZB Bank",
     *      description="Bank name"
     * )
     * 
     * @var integer
     */
    private $bank_name;

    /**
     * @OA\Property(
     *      title="Account Name",
     *      example="Milton Gore",
     *      description="Account Name"
     * )
     * 
     * @var string
     */
    private $account_name;

    /**
     * @OA\Property(
     *      title="Account Number",
     *      example="1000000000054",
     *      description="Account Number"
     * )
     * 
     * @var string
     */
    private $account_number;

    /**
     * @OA\Property(
     *      title="Staff ID",
     *      example="1",
     *      description="Staff ID"
     * )
     * 
     * @var integer
     */
    private $staff_id;
}
