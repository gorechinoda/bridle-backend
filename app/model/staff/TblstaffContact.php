<?php

namespace App\model\staff;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="StaffContact",
 *      @OA\Xml(
 *          name="StaffContact",
 *      )
 * )
 */
class TblstaffContact extends Model
{
    //
    protected $fillable =[
        'id', 'staff_id', 'email', 'mobile', 'address', 'post_code', 'city', 'state', 'country',
    ];
    
    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Staff ID",
     *      example="1",
     *      description="Staff ID"
     * )
     * 
     * @var integer
     */
    private $staff_id;

    /**
     * @OA\Property(
     *      title="Email",
     *      example="test@gmail.com",
     *      description="Email"
     * )
     * 
     * @var string
     */
    private $emai;

    /**
     * @OA\Property(
     *      title="Mobile",
     *      example="0718171898",
     *      description="Mobile"
     * )
     * 
     * @var string
     */
    private $mobile;

    /**
     * @OA\Property(
     *      title="Address",
     *      example="Kumba kwedu boe manje",
     *      description="Address"
     * )
     * 
     * @var string
     */
    private $address;

    /**
     * @OA\Property(
     *      title="Post code",
     *      example="00263",
     *      description="Postal code"
     * )
     * 
     * @var integer
     */
    private $post_code;

    /**
     * @OA\Property(
     *      title="City",
     *      example="Harare",
     *      description="City of residence"
     * )
     * 
     * @var string
     */
    private $city;

    /**
     * @OA\Property(
     *      title="State",
     *      example="Masvingo Province",
     *      description="State of residence"
     * )
     * 
     * @var string
     */
    private $state;

    /**
     * @OA\Property(
     *      title="Country",
     *      example="Zimbabwe",
     *      description="Country"
     * )
     * 
     * @var string
     */
    private $country;
}
