<?php

namespace App\model\staff;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="StaffDocument",
 *      @OA\Xml(
 *          name="StaffDocument",
 *      )
 * )
 */
class TblstaffDocument extends Model
{
    //
    protected $fillable = [
        'id', 'document_type_id', 'file_url', 'staff_id',
    ];

    public function documentType()
    {
        return $this->belongsTo('App\model\staff\TblstaffDocumentType', 'document_type_id', 'id');
    }

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Document type ID",
     *      example="1",
     *      description="Document type ID"
     * )
     * 
     * @var integer
     */
    private $document_type_id;

    /**
     * @OA\Property(
     *      title="File Url",
     *      example="storage/documentname.type",
     *      description="This url comes holding a file during post then it converted to url during the document Saving"
     * )
     * 
     * @var string
     */
    private $file_url;

    /**
     * @OA\Property(
     *      title="Staff ID",
     *      example="1",
     *      description="Staff ID"
     * )
     * 
     * @var integer
     */
    private $staff_id;

    /**
     * @OA\Property(
     *      title="Staff ID",
     *      example="1",
     *      description="Staff ID"
     * )
     * 
     * @var \App\model\staff\TblstaffDocumentType
     */
    private $documentType;
}
