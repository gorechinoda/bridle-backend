<?php

namespace App\model\staff;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="StaffDocumentType",
 *      @OA\Xml(
 *          name="StaffDocumentType",
 *      )
 * )
 */
class TblstaffDocumentType extends Model
{
    //
    protected $fillable = [
        'id', 'name',
    ];

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Name",
     *      example="Ndyp",
     *      description="Name"
     * )
     * 
     * @var string
     */
    private $name;
}
