<?php

namespace App\model\staff;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="StaffEducation",
 *      @OA\Xml(
 *          name="StaffEducation",
 *      )
 * )
 */
class TblstaffEducation extends Model
{
    //
    protected $fillable = [
        'id', 'staff_id', 'school_name', 'education_level_id', 'field_of_study', 'date_of_completion',
    ];

    public function educationLevel()
    {
        return $this->belongsTo('App\model\staff\TblstaffEducationLevel', 'education_level_id', 'id');
    }

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Staff ID",
     *      example="1",
     *      description="Staff ID"
     * )
     * 
     * @var integer
     */
    private $staff_id;

    /**
     * @OA\Property(
     *      title="School name ",
     *      example="1",
     *      description="School name"
     * )
     * 
     * @var string
     */
    private $school_name;

    /**
     * @OA\Property(
     *      title="Education Level ID",
     *      example="1",
     *      description="Education Level ID"
     * )
     * 
     * @var integer
     */
    private $education_level_id;

    /**
     * @OA\Property(
     *      title="Field of Study",
     *      example="Information Technology",
     *      description="Field of study"
     * )
     * 
     * @var string
     */
    private $field_of_study;

    /**
     * @OA\Property(
     *      title="Date of Completion",
     *      example="2020-12-31",
     *      description="Date of completion masinhi"
     * )
     * 
     * @var string
     */
    private $date_of_completion;

    /**
     * @OA\Property(
     *      title="Document type ID",
     *      example="1",
     *      description="Document type ID"
     * )
     * 
     * @var \App\model\staff\TblstaffEducationLevel
     */
    private $educationLevel;

}
