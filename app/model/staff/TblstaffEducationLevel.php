<?php

namespace App\model\staff;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="StaffEducationLevel",
 *      @OA\Xml(
 *          name="StaffEducationLevel",
 *      )
 * )
 */
class TblstaffEducationLevel extends Model
{
    //
    protected $fillable = [
        'id', 'name'
    ];

     /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

     /**
     * @OA\Property(
     *      title="Name",
     *      example="Form 6",
     *      description="Name"
     * )
     * 
     * @var integer
     */
    private $name;
}
