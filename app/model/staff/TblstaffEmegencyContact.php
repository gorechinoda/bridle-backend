<?php

namespace App\model\staff;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="StaffEmegencyContact",
 *      @OA\Xml(
 *          name="StaffEmegencyContact",
 *      )
 * )
 */
class TblstaffEmegencyContact extends Model
{
    //
    protected $fillable = [
        'id', 'contact_name', 'phone', 'email', 'relationship', 'address', 'staff_id',
    ];

     /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

     /**
     * @OA\Property(
     *      title="Contact name",
     *      example="Jack",
     *      description="Contact name"
     * )
     * 
     * @var string
     */
    private $contact_name;

     /**
     * @OA\Property(
     *      title="Phone",
     *      example="0777906994",
     *      description="Phone number"
     * )
     * 
     * @var string
     */
    private $phone;

     /**
     * @OA\Property(
     *      title="Email",
     *      example="cla@gmail.com",
     *      description="Email"
     * )
     * 
     * @var string
     */
    private $email;

     /**
     * @OA\Property(
     *      title="Relationship",
     *      example="1",
     *      description="Relationship"
     * )
     * 
     * @var string
     */
    private $relationship;

     /**
     * @OA\Property(
     *      title="Address",
     *      example="Magunje Harere Zimbebwe",
     *      description="Address"
     * )
     * 
     * @var string
     */
    private $address;

     /**
     * @OA\Property(
     *      title="Staff ID",
     *      example="1",
     *      description="Staff ID"
     * )
     * 
     * @var integer
     */
    private $staff_id;


}
