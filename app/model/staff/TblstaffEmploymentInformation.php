<?php

namespace App\model\staff;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="StaffEmploymentInformation",
 *      @OA\Xml(
 *          name="StaffEmploymentInformation",
 *      )
 * )
 */
class TblstaffEmploymentInformation extends Model
{
    //
    protected $fillable = [
        'id', 'staff_id', 'date_of_employment', 'end_date_of_employment', 'speciality',
    ];

     /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

     /**
     * @OA\Property(
     *      title="Staff ID",
     *      example="1",
     *      description="Staff ID"
     * )
     * 
     * @var integer
     */
    private $staff_id;

     /**
     * @OA\Property(
     *      title="Date of Employment",
     *      example="2019-12-01",
     *      description="Date of Employment"
     * )
     * 
     * @var date
     */
    private $date_of_employment;

     /**
     * @OA\Property(
     *      title="End Date of Employment",
     *      example="2020-12-31",
     *      description="End date of employment"
     * )
     * 
     * @var date
     */
    private $end_date_of_employment;

     /**
     * @OA\Property(
     *      title="Speciality",
     *      example="Programmer",
     *      description="Speciality"
     * )
     * 
     * @var string
     */
    private $speciality;
}
