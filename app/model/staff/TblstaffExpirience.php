<?php

namespace App\model\staff;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="StaffExpirience",
 *      @OA\Xml(
 *          name="StaffExpirience",
 *      )
 * )
 */
class TblstaffExpirience extends Model
{
    //
    protected $fillable = [
        'id', 'staff_id', 'occupation', 'company', 'summary', 'start_date', 'end_date',
    ];

     /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

     /**
     * @OA\Property(
     *      title="Staff ID",
     *      example="1",
     *      description="Staff ID"
     * )
     * 
     * @var integer
     */
    private $staff_id;

     /**
     * @OA\Property(
     *      title="Occupation",
     *      example="Data clerk",
     *      description="Occupation"
     * )
     * 
     * @var string
     */
    private $occupation;

     /**
     * @OA\Property(
     *      title="Company",
     *      example="Chinhoyi University Of Technology",
     *      description="Company"
     * )
     * 
     * @var string
     */
    private $company;

     /**
     * @OA\Property(
     *      title="Summary",
     *      example="Capturing Bridle data",
     *      description="Summary"
     * )
     * 
     * @var string
     */
    private $summary;

     /**
     * @OA\Property(
     *      title="Start Date",
     *      example="1999-01-31",
     *      description="Start Date"
     * )
     * 
     * @var date
     */
    private $start_date;

     /**
     * @OA\Property(
     *      title="End date",
     *      example="2009-12-31",
     *      description="End Date"
     * )
     * 
     * @var date
     */
    private $end_date;
}
