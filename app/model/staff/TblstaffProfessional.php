<?php

namespace App\model\staff;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="StaffProfessional",
 *      @OA\Xml(
 *          name="StaffProfessional",
 *      )
 * )
 */
class TblstaffProfessional extends Model
{
    //
    protected $fillable = [
        'id', 'staff_id', 'expirience', 'source_of_hire', 'current_salary', 'offer_letter_url',
        'skillset', 'highest_qualification', 'additional_info',
    ];

     /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

     /**
     * @OA\Property(
     *      title="Staff ID",
     *      example="1",
     *      description="staff ID"
     * )
     * 
     * @var integer
     */
    private $staff_id;

     /**
     * @OA\Property(
     *      title="Expirience",
     *      example="1",
     *      description="Number of month of expirience"
     * )
     * 
     * @var integer
     */
    private $expirience;

     /**
     * @OA\Property(
     *      title="Source of Hire",
     *      example="FB",
     *      description="Source of hire"
     * )
     * 
     * @var string
     */
    private $source_of_hire;

     /**
     * @OA\Property(
     *      title="Current Salary",
     *      example="100000.99",
     *      description="Current salary"
     * )
     * 
     * @var double
     */
    private $current_salary;

     /**
     * @OA\Property(
     *      title="offer letter url",
     *      example="storage/filename.pdf",
     *      description="offer letter url, comes as a file during creation and then converted to url"
     * )
     * 
     * @var string
     */
    private $offer_letter_url;

     /**
     * @OA\Property(
     *      title="Skillset",
     *      example="English, Java, Php, Sql modeling",
     *      description="Skillset"
     * )
     * 
     * @var string
     */
    private $skillset;

     /**
     * @OA\Property(
     *      title="Highest Qualification",
     *      example="Degree",
     *      description="Highest Qualification"
     * )
     * 
     * @var string
     */
    private $highest_qualification;

     /**
     * @OA\Property(
     *      title="Additional Information",
     *      example="Haaa ndaneta",
     *      description="Additional Information"
     * )
     * 
     * @var string
     */
    private $additional_info;

}
