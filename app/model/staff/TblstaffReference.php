<?php

namespace App\model\staff;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="StaffReference",
 *      @OA\Xml(
 *          name="StaffReference",
 *      )
 * )
 */
class TblstaffReference extends Model
{
    //
    protected $fillable = [
        'id', 'staff_id', 'contact_name', 'contact_phone', 'contact_email',
    ];

     /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

     /**
     * @OA\Property(
     *      title="Staff ID",
     *      example="1",
     *      description="Staff ID"
     * )
     * 
     * @var integer
     */
    private $staff_id;

     /**
     * @OA\Property(
     *      title="Contact name",
     *      example="Done",
     *      description="Cone name"
     * )
     * 
     * @var string
     */
    private $contact_name;

     /**
     * @OA\Property(
     *      title="Contact phone",
     *      example="0123456789",
     *      description="Contact phone"
     * )
     * 
     * @var string
     */
    private $contact_phone;

     /**
     * @OA\Property(
     *      title="Contact Email",
     *      example="wow@gmail.com",
     *      description="Contact Email"
     * )
     * 
     * @var string
     */
    private $contact_email;
}
