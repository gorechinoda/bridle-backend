<?php

namespace App\model\support;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="SupportArea",
 *      @OA\Xml(
 *          name="SupportArea",
 *      )
 * )
 */
class TblsupportArea extends Model
{
    //
    protected $fillable = [
        'id','name','company_id','status'
    ];

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

     /**
     * @OA\Property(
     *      title="Name",
     *      example="Wow",
     *      description="Name"
     * )
     * 
     * @var string
     */
    private $name;

     /**
     * @OA\Property(
     *      title="Company ID",
     *      example="1",
     *      description="Company ID"
     * )
     * 
     * @var integer
     */
    private $company_id;

     /**
     * @OA\Property(
     *      title="Status",
     *      example="true",
     *      description="Status"
     * )
     * 
     * @var bool
     */
    private $status;
}
