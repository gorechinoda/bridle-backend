<?php

namespace App\model\support;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="SupportService",
 *      @OA\Xml(
 *          name="SupportService",
 *      )
 * )
 */
class TblsupportService extends Model
{
    //
    protected $fillable = [
        'id','support_area_id','item_number','price','name','status',
    ];

    public function supportArea()
    {
        return $this->belongsTo('App\model\support\TblsupportArea', 'support_area_id', 'id');
    }

    /**
     * @OA\Property(
     *      title="ID",
     *      example="1",
     *      description="ID"
     * )
     * 
     * @var integer
     */
    private $id;

     /**
     * @OA\Property(
     *      title="Support area ID",
     *      example="1",
     *      description="Support area ID"
     * )
     * 
     * @var integer
     */
    private $support_area_id;

     /**
     * @OA\Property(
     *      title="Item Number",
     *      example="1",
     *      description="Item Number"
     * )
     * 
     * @var integer
     */
    private $item_number;

     /**
     * @OA\Property(
     *      title="Price",
     *      example="100.45",
     *      description="Price"
     * )
     * 
     * @var double
     */
    private $price;

     /**
     * @OA\Property(
     *      title="Name",
     *      example="Milton",
     *      description="Name"
     * )
     * 
     * @var string
     */
    private $name;

     /**
     * @OA\Property(
     *      title="Status",
     *      example="true",
     *      description="Status"
     * )
     * 
     * @var bool
     */
    private $status;

     /**
     * @OA\Property(
     *      title="Support area",
     *      description="Support Area"
     * )
     * 
     * @var \App\model\support\TblsupportArea
     */
    private $supportArea;
}
