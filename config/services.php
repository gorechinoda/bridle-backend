<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
<<<<<<< HEAD
    | as Mailgun, SparkPost and others. This file provides a sane default
=======
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
>>>>>>> 80474dc5f730799d850b9c361988349b8b02d79a
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

// <<<<<<< HEAD
    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

// =======
// >>>>>>> 80474dc5f730799d850b9c361988349b8b02d79a
];
