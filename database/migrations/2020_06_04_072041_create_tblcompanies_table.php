<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblcompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblcompanies', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->string('company_name');
            $table->string('company_email')->unique();
            $table->string('company_mobile');
            $table->integer('status')->default(0);
            $table->integer('employee_range_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblcompanies');
    }
}
