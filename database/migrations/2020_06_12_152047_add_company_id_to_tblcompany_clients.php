<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyIdToTblcompanyClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblcompany_clients', function (Blueprint $table) {
            //
            $table->integer("company_id");
            //$table->foreign("company_id")->references("id")->on("tblcompanies")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblcompany_clients', function (Blueprint $table) {
            //
        });
    }
}
