<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblclientPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblclient_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date("start_date");
            $table->date("end_date");
            $table->integer("plan_type_id");
            $table->string("ndis_number");
            $table->integer("client_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblclient_plans');
    }
}
