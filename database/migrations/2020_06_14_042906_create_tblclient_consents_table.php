<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblclientConsentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblclient_consents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("client");
            $table->string("person");
            $table->date("expiry_date");
            $table->integer("consent_type_id");
            $table->integer("client_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblclient_consents');
    }
}
