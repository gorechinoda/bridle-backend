<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbladditionalContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbladditional_contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("client_id");
            $table->integer("contact_type_id");
            $table->string("name");
            $table->string("home_phone")->nullable();
            $table->string("work_phone")->nullable();
            $table->text("comment")->nullable();
            $table->string("mobile_phone")->nullable();
            $table->string("postal_address")->nullable();
            $table->string("email")->nullable();
            $table->string("relationship_contact_method");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbladditional_contacts');
    }
}
