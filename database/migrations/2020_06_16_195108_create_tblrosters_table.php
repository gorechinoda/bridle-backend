<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblrostersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblrosters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("company_id");
            $table->integer("client_id");
            $table->integer("staff_id");
            // $table->integer("approved_id");
            // $table->integer("confirmed_id");
            $table->integer("service_id");
            $table->integer("status");
            $table->dateTime("sdt");
            $table->dateTime("edt");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblrosters');
    }
}
