<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblstaffEmploymentInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblstaff_employment_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('staff_id');
            $table->date('date_of_employment')->nullable();
            $table->date('end_date_of_employment')->nullable();
            $table->string('speciality')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblstaff_employment_information');
    }
}
