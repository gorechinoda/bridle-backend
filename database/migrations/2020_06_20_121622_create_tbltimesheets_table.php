<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbltimesheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbltimesheets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("company_id");
            $table->integer("staff_id");
            $table->time("start_time");
            $table->time("end_time");
            $table->date("start_date");
            $table->date("end_date");
            $table->time("break_start_at");
            $table->time("break_end_at");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbltimesheets');
    }
}
