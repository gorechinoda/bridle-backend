<?php

use Illuminate\Database\Seeder;

class TblemplyeeRange extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tblemplyee_ranges')->insert([
            'name' => "1-10",
        ]);
        DB::table('tblemplyee_ranges')->insert([
            'name' => "11-50",
        ]);
        DB::table('tblemplyee_ranges')->insert([
            'name' => "51-100",
        ]);
    }
}
