<?php

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('login', 'TblcompanyStaffController@login');//done

Route::prefix("company")->group(function(){
    // Route::resource('', 'TblcompanyController');

    Route::get('','TblcompanyController@index');
    Route::post('','TblcompanyController@store');
    Route::get('create','TblcompanyController@create');
    Route::get('{id}','TblcompanyController@show');
    Route::put('{id}','TblcompanyController@update');
    Route::delete('{id}','TblcompanyController@destroy');
    Route::get('{id}/edit','TblcompanyController@edit');

    Route::post('check', 'TblcompanyController@checkCompany');//done
    Route::get('{id}/staff',"TblcompanyController@getCompanyStaff");
    Route::get("{id}/clients","TblcompanyController@getCompanyClients");
    Route::get("{id}/licences","TblcompanyController@getCompanyLicense");
    Route::get("{id}/timesheets","TblcompanyController@getCompanyTimesheets");
    Route::get("{id}/events","TblcompanyController@getCompanyEvents");
});

Route::prefix("client")->group(function(){
    Route::get('','TblcompanyClientController@index');
    Route::post('','TblcompanyClientController@store');//done
    Route::get('create','TblcompanyClientController@create');
    Route::get('{id}','TblcompanyClientController@show');
    Route::put('{id}','TblcompanyClientController@update');
    Route::delete('{id}','TblcompanyClientController@destroy');
    Route::delete('{id}/byUserId','TblcompanyClientController@destroyByUserId');
    Route::get('{id}/edit','TblcompanyClientController@edit');
});


Route::prefix("companyStaff")->group(function(){
    Route::get('', 'TblcompanyStaffController@index');
    Route::post('','TblcompanyStaffController@store');//done
    Route::post('/selfRegister/{companyEmail}','TblcompanyStaffController@selfRegister');//done
    Route::get("/{id}","TblcompanyStaffController@show");
    Route::put("/{id}","TblcompanyStaffController@updateStaff");
    Route::put("/changePassword","TblcompanyStaffController@changePassword");
    Route::delete('{id}/remove',"TblcompanyStaffController@delete");
    Route::delete('byUserId/{id}',"TblcompanyStaffController@deleteByUserId");

    //  TblstaffContact
    Route::prefix('contact')->group(function(){
        Route::get('','TblcompanyStaffController@createStaffContact');
        Route::post('','TblcompanyStaffController@saveStaffContact');//Done
        Route::put('','TblcompanyStaffController@updateStaffContact');
        Route::delete('{id}','TblcompanyStaffController@destroyStaffContact');
        Route::get('for/{staffId}','TblcompanyStaffController@geStafftContactsFor');
    });

    // TblstaffProfessional;
    Route::prefix('professional')->group(function(){
        Route::get('','TblcompanyStaffController@createStaffProfessional');
        Route::post('','TblcompanyStaffController@saveStaffProfessional');//Done
        Route::put('','TblcompanyStaffController@updateStaffProfessional');
        Route::delete('{id}','TblcompanyStaffController@destroyStaffProfession');
        Route::get('for/{staffId}','TblcompanyStaffController@getStaffProfessionalsFor');
    });


    // TblstaffEducation;
    Route::prefix('education')->group(function(){
        Route::get('','TblcompanyStaffController@createStaffEducation');
        Route::post('','TblcompanyStaffController@saveStaffEducation');//Done
        Route::put('','TblcompanyStaffController@updateStaffEducation');
        Route::delete('{id}','TblcompanyStaffController@destroyStaffEducation');
        Route::get('for/{staffId}','TblcompanyStaffController@getStaffEducationFor');
    });


    // TblstaffExpirience;
    Route::prefix('expirience')->group(function(){
        Route::get('','TblcompanyStaffController@createStaffExpirience');
        Route::post('','TblcompanyStaffController@saveStaffExpirience');//done
        Route::put('','TblcompanyStaffController@updateStaffExpirience');
        Route::delete('{id}','TblcompanyStaffController@destroyStaffExpirience');
        Route::get('for/{staffId}','TblcompanyStaffController@getStaffExpirienceFor');
    });


    // TblstaffDocument;
    Route::prefix('document')->group(function(){
        Route::get('','TblcompanyStaffController@createStaffDocument');
        Route::post('','TblcompanyStaffController@saveStaffDocument');//done
        Route::put('','TblcompanyStaffController@updateStaffDocument');
        Route::delete('{id}','TblcompanyStaffController@destroyStaffDocument');
        Route::get('for/{staffId}','TblcompanyStaffController@getStaffDocumentsFor');
    });


    // TblstaffEmploymentInformation;
    Route::prefix('employmentInformation')->group(function(){
        Route::get('','TblcompanyStaffController@createStaffEmploymentInformation');
        Route::post('','TblcompanyStaffController@saveStaffEmploymentInformation');//done
        Route::put('','TblcompanyStaffController@updateStaffEmploymentInformation');
        Route::delete('{id}','TblcompanyStaffController@destroyStaffEmploymentInformation');
        Route::get('for/{staffId}','TblcompanyStaffController@getStaffEmploymentInformationFor');

    });

    //  TblstaffBankingInformation;
    Route::prefix('bankingInformation')->group(function(){
        Route::get('','TblcompanyStaffController@createStaffBankingInformation');
        Route::post('','TblcompanyStaffController@saveStaffBankingInformation');//done
        Route::put('','TblcompanyStaffController@updateStaffBankingInformation');
        Route::delete('{id}','TblcompanyStaffController@destroyStaffBankingInformation');
        Route::get('for/{staffId}','TblcompanyStaffController@getStaffBankingInformationFor');
    });

    //  TblstaffReference;
    Route::prefix('reference')->group(function(){
        Route::get('','TblcompanyStaffController@createStaffReference');
        Route::post('','TblcompanyStaffController@saveStaffReference');//done
        Route::put('','TblcompanyStaffController@updateStaffReference');
        Route::delete('{id}','TblcompanyStaffController@destroyStaffReference');
        Route::get('for/{staffId}','TblcompanyStaffController@getStaffReferenceFor');
    });


    //  TblstaffEmegencyContact;
    Route::prefix('emegencyContact')->group(function(){
        Route::get('','TblcompanyStaffController@createStaffEmegencyContact');
        Route::post('','TblcompanyStaffController@saveStaffEmegencyContact');//done
        Route::put('','TblcompanyStaffController@updateStaffEmegencyContact');
        Route::delete('{id}','TblcompanyStaffController@destroyStaffEmegencyContact');
        Route::get('for/{staffId}','TblcompanyStaffController@getStaffEmegencyContactsFor');
    });


});

Route::prefix("location")->group(function(){
    Route::resource('city',"location\CityController");
    Route::resource("country","location\CountryController");
    Route::resource("province","location\ProvinceController");
    Route::resource("continent","location\ContinentController");
});


//Events Api
Route::resource('events', 'TbleventController');//documented put, post


//Timesheet api
Route::resource("timesheet",'TbltimesheetController'); //documented put, post

Route::prefix("roster")->group(function(){
    Route::post("","roster\RosterController@create");//documented
    Route::put('{id}',"roster\RosterController@update");//documented
});
